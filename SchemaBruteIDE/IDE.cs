// Copyright � 2009 Phil Booth
//
// This file is part of SchemaBruteIDE.
//
// SchemaBruteIDE is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// SchemaBruteIDE is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SchemaBruteIDE. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ScintillaNet;

namespace SchemaBruteIDE
{
	public partial class IDE : Form, View
	{
		public IDE()
		{
			InitializeComponent();

			controller = new Controller(this);
		}

		string View.SelectSchema()
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.AutoUpgradeEnabled = true;
			dialog.Filter = "XML schemata (*.xsd)|*.xsd|All files (*.*)|*.*";
			dialog.RestoreDirectory = true;
			dialog.SupportMultiDottedExtensions = true;
			dialog.Title = "Open schema";
			if(dialog.ShowDialog(this) == DialogResult.OK)
				return dialog.FileName;

			return "";
		}

		void View.LoadSchema(string schemaPath, byte[] schema)
		{
			TabPage schemaTab = new TabPage(Path.GetFileName(schemaPath));
			schemaTab.Size = tabs.ClientSize;
			schemaTab.ToolTipText = schemaPath;
			schemaTab.UseVisualStyleBackColor = true;
			tabs.TabPages.Add(schemaTab);
			tabs.SelectedTab = schemaTab;
			Scintilla scintilla = InstantiateScintilla();
			scintilla.Parent = schemaTab;
			scintilla.RawText = schema;
		}

		private Scintilla InstantiateScintilla()
		{
			// TODO: Read preferences from disk, via Controller and Preferences instances.
			Scintilla scintilla = new Scintilla();
			scintilla.AcceptsReturn = true;
			scintilla.AcceptsTab = true;
			scintilla.AllowDrop = true;
			scintilla.Dock = DockStyle.Fill;
			scintilla.ConfigurationManager.CustomLocation = "ScintillaNET.xml";
			scintilla.ConfigurationManager.Language = "xml";
			scintilla.Enabled = true;
			// TODO: Investigate indentation, do it.
			//scintilla.Indentation = SmartIndent.Simple;
			// TODO: Investigate what is required to claim accessibility, then do it.
			scintilla.IsAccessible = false;
			scintilla.IsReadOnly = false;
			scintilla.Location = new Point(0);
			scintilla.MatchBraces = true;
			scintilla.Margin = scintilla.Padding = new Padding(0);
			// TODO: Calculate the width of the line number margin dynamically.
			scintilla.Margins[0].Width = 40;
			scintilla.OverType = false;
			scintilla.Styles.LineNumber.BackColor = Color.Blue;
			scintilla.Styles.LineNumber.ForeColor = Color.Cyan;
			scintilla.TabIndex = 0;
			scintilla.TabStop = false;
			scintilla.Visible = true;
			return scintilla;
		}

		private void openSchemaMenuItem_Click(object sender, EventArgs evt)
		{
			controller.OpenSchema();
		}

		private void tabs_MouseClick(object sender, MouseEventArgs evt)
		{
			if(evt.Button == MouseButtons.Right)
			{
				for(int i = 0; i < tabs.TabPages.Count; ++i)
				{
					if(tabs.GetTabRect(i).Contains(evt.Location))
					{
						tabs.SelectedIndex = i;
						break;
					}
				}
				tabContextMenu.Show(tabs, evt.Location);
			}
		}

		private Controller controller;
	}
}
