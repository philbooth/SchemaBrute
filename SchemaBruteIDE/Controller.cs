// Copyright � 2009 Phil Booth
//
// This file is part of SchemaBruteIDE.
//
// SchemaBruteIDE is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// SchemaBruteIDE is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SchemaBruteIDE. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace SchemaBruteIDE
{
	class Controller
	{
		public Controller(View viewArg)
		{
			view = viewArg;
		}

		public void OpenSchema()
		{
			string schemaPath = view.SelectSchema();
			if(schemaPath != "")
			{
				byte[] schema = File.ReadAllBytes(schemaPath);
				view.LoadSchema(schemaPath, schema);
			}
		}

		private Controller()
		{
		}

		private View view;
	}
}
