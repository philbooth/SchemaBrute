namespace SchemaBruteIDE
{
	partial class IDE
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.statusbar = new System.Windows.Forms.StatusStrip();
			this.menubar = new System.Windows.Forms.MenuStrip();
			this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.fileNewMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.newProjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newSchemaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fileOpenMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.openProjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openSchemaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fileMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fileMenuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.fileCloseMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.closeFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.closeProjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fileMenuSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.undoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.redoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.copyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pasteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editMenuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.selectAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editMenuSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.findReplaceMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.findMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.replaceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buildMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolbar = new System.Windows.Forms.ToolStrip();
			this.startPageTab = new System.Windows.Forms.TabPage();
			this.startPageView = new System.Windows.Forms.WebBrowser();
			this.tabs = new System.Windows.Forms.TabControl();
			this.tabContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tabSaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabCloseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabCloseAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.copToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.yToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabMenuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.openContainingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menubar.SuspendLayout();
			this.startPageTab.SuspendLayout();
			this.tabs.SuspendLayout();
			this.tabContextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// statusbar
			// 
			this.statusbar.Location = new System.Drawing.Point(0, 542);
			this.statusbar.Name = "statusbar";
			this.statusbar.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
			this.statusbar.Size = new System.Drawing.Size(784, 22);
			this.statusbar.TabIndex = 0;
			// 
			// menubar
			// 
			this.menubar.Dock = System.Windows.Forms.DockStyle.None;
			this.menubar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.buildMenu,
            this.helpMenu});
			this.menubar.Location = new System.Drawing.Point(0, 0);
			this.menubar.Name = "menubar";
			this.menubar.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
			this.menubar.Size = new System.Drawing.Size(175, 24);
			this.menubar.TabIndex = 0;
			// 
			// fileMenu
			// 
			this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileNewMenu,
            this.fileOpenMenu,
            this.fileMenuSeparator1,
            this.saveMenuItem,
            this.saveAsMenuItem,
            this.saveAllMenuItem,
            this.fileMenuSeparator2,
            this.fileCloseMenu,
            this.fileMenuSeparator3,
            this.exitMenuItem});
			this.fileMenu.Name = "fileMenu";
			this.fileMenu.Size = new System.Drawing.Size(37, 20);
			this.fileMenu.Text = "&File";
			// 
			// fileNewMenu
			// 
			this.fileNewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectMenuItem,
            this.newSchemaMenuItem,
            this.newFileMenuItem});
			this.fileNewMenu.Name = "fileNewMenu";
			this.fileNewMenu.Size = new System.Drawing.Size(121, 22);
			this.fileNewMenu.Text = "&New";
			// 
			// newProjectMenuItem
			// 
			this.newProjectMenuItem.Name = "newProjectMenuItem";
			this.newProjectMenuItem.Size = new System.Drawing.Size(125, 22);
			this.newProjectMenuItem.Text = "&Project...";
			// 
			// newSchemaMenuItem
			// 
			this.newSchemaMenuItem.Name = "newSchemaMenuItem";
			this.newSchemaMenuItem.Size = new System.Drawing.Size(125, 22);
			this.newSchemaMenuItem.Text = "&Schema...";
			// 
			// newFileMenuItem
			// 
			this.newFileMenuItem.Name = "newFileMenuItem";
			this.newFileMenuItem.Size = new System.Drawing.Size(125, 22);
			this.newFileMenuItem.Text = "&File...";
			// 
			// fileOpenMenu
			// 
			this.fileOpenMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openProjectMenuItem,
            this.openSchemaMenuItem,
            this.openFileMenuItem});
			this.fileOpenMenu.Name = "fileOpenMenu";
			this.fileOpenMenu.Size = new System.Drawing.Size(121, 22);
			this.fileOpenMenu.Text = "&Open";
			// 
			// openProjectMenuItem
			// 
			this.openProjectMenuItem.Name = "openProjectMenuItem";
			this.openProjectMenuItem.Size = new System.Drawing.Size(125, 22);
			this.openProjectMenuItem.Text = "&Project...";
			// 
			// openSchemaMenuItem
			// 
			this.openSchemaMenuItem.Name = "openSchemaMenuItem";
			this.openSchemaMenuItem.Size = new System.Drawing.Size(125, 22);
			this.openSchemaMenuItem.Text = "&Schema...";
			this.openSchemaMenuItem.Click += new System.EventHandler(this.openSchemaMenuItem_Click);
			// 
			// openFileMenuItem
			// 
			this.openFileMenuItem.Name = "openFileMenuItem";
			this.openFileMenuItem.Size = new System.Drawing.Size(125, 22);
			this.openFileMenuItem.Text = "&File...";
			// 
			// fileMenuSeparator1
			// 
			this.fileMenuSeparator1.Name = "fileMenuSeparator1";
			this.fileMenuSeparator1.Size = new System.Drawing.Size(118, 6);
			// 
			// saveMenuItem
			// 
			this.saveMenuItem.Name = "saveMenuItem";
			this.saveMenuItem.Size = new System.Drawing.Size(121, 22);
			this.saveMenuItem.Text = "&Save";
			// 
			// saveAsMenuItem
			// 
			this.saveAsMenuItem.Name = "saveAsMenuItem";
			this.saveAsMenuItem.Size = new System.Drawing.Size(121, 22);
			this.saveAsMenuItem.Text = "Save &as...";
			// 
			// saveAllMenuItem
			// 
			this.saveAllMenuItem.Name = "saveAllMenuItem";
			this.saveAllMenuItem.Size = new System.Drawing.Size(121, 22);
			this.saveAllMenuItem.Text = "Save A&ll";
			// 
			// fileMenuSeparator2
			// 
			this.fileMenuSeparator2.Name = "fileMenuSeparator2";
			this.fileMenuSeparator2.Size = new System.Drawing.Size(118, 6);
			// 
			// fileCloseMenu
			// 
			this.fileCloseMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeFileMenuItem,
            this.closeProjectMenuItem});
			this.fileCloseMenu.Name = "fileCloseMenu";
			this.fileCloseMenu.Size = new System.Drawing.Size(121, 22);
			this.fileCloseMenu.Text = "&Close";
			// 
			// closeFileMenuItem
			// 
			this.closeFileMenuItem.Name = "closeFileMenuItem";
			this.closeFileMenuItem.Size = new System.Drawing.Size(111, 22);
			this.closeFileMenuItem.Text = "&File";
			// 
			// closeProjectMenuItem
			// 
			this.closeProjectMenuItem.Name = "closeProjectMenuItem";
			this.closeProjectMenuItem.Size = new System.Drawing.Size(111, 22);
			this.closeProjectMenuItem.Text = "&Project";
			// 
			// fileMenuSeparator3
			// 
			this.fileMenuSeparator3.Name = "fileMenuSeparator3";
			this.fileMenuSeparator3.Size = new System.Drawing.Size(118, 6);
			// 
			// exitMenuItem
			// 
			this.exitMenuItem.Name = "exitMenuItem";
			this.exitMenuItem.Size = new System.Drawing.Size(121, 22);
			this.exitMenuItem.Text = "E&xit";
			// 
			// editMenu
			// 
			this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoMenuItem,
            this.redoMenuItem,
            this.editMenuSeparator1,
            this.copyMenuItem,
            this.cutMenuItem,
            this.pasteMenuItem,
            this.deleteMenuItem,
            this.editMenuSeparator2,
            this.selectAllMenuItem,
            this.editMenuSeparator3,
            this.findReplaceMenu});
			this.editMenu.Name = "editMenu";
			this.editMenu.Size = new System.Drawing.Size(39, 20);
			this.editMenu.Text = "&Edit";
			// 
			// undoMenuItem
			// 
			this.undoMenuItem.Name = "undoMenuItem";
			this.undoMenuItem.Size = new System.Drawing.Size(161, 22);
			this.undoMenuItem.Text = "&Undo";
			// 
			// redoMenuItem
			// 
			this.redoMenuItem.Name = "redoMenuItem";
			this.redoMenuItem.Size = new System.Drawing.Size(161, 22);
			this.redoMenuItem.Text = "&Redo";
			// 
			// editMenuSeparator1
			// 
			this.editMenuSeparator1.Name = "editMenuSeparator1";
			this.editMenuSeparator1.Size = new System.Drawing.Size(158, 6);
			// 
			// copyMenuItem
			// 
			this.copyMenuItem.Name = "copyMenuItem";
			this.copyMenuItem.Size = new System.Drawing.Size(161, 22);
			this.copyMenuItem.Text = "&Copy";
			// 
			// cutMenuItem
			// 
			this.cutMenuItem.Name = "cutMenuItem";
			this.cutMenuItem.Size = new System.Drawing.Size(161, 22);
			this.cutMenuItem.Text = "Cu&t";
			// 
			// pasteMenuItem
			// 
			this.pasteMenuItem.Name = "pasteMenuItem";
			this.pasteMenuItem.Size = new System.Drawing.Size(161, 22);
			this.pasteMenuItem.Text = "&Paste";
			// 
			// deleteMenuItem
			// 
			this.deleteMenuItem.Name = "deleteMenuItem";
			this.deleteMenuItem.Size = new System.Drawing.Size(161, 22);
			this.deleteMenuItem.Text = "&Delete";
			// 
			// editMenuSeparator2
			// 
			this.editMenuSeparator2.Name = "editMenuSeparator2";
			this.editMenuSeparator2.Size = new System.Drawing.Size(158, 6);
			// 
			// selectAllMenuItem
			// 
			this.selectAllMenuItem.Name = "selectAllMenuItem";
			this.selectAllMenuItem.Size = new System.Drawing.Size(161, 22);
			this.selectAllMenuItem.Text = "Select &all";
			// 
			// editMenuSeparator3
			// 
			this.editMenuSeparator3.Name = "editMenuSeparator3";
			this.editMenuSeparator3.Size = new System.Drawing.Size(158, 6);
			// 
			// findReplaceMenu
			// 
			this.findReplaceMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findMenuItem,
            this.replaceMenuItem});
			this.findReplaceMenu.Name = "findReplaceMenu";
			this.findReplaceMenu.Size = new System.Drawing.Size(161, 22);
			this.findReplaceMenu.Text = "&Find and replace";
			// 
			// findMenuItem
			// 
			this.findMenuItem.Name = "findMenuItem";
			this.findMenuItem.Size = new System.Drawing.Size(115, 22);
			this.findMenuItem.Text = "&Find";
			// 
			// replaceMenuItem
			// 
			this.replaceMenuItem.Name = "replaceMenuItem";
			this.replaceMenuItem.Size = new System.Drawing.Size(115, 22);
			this.replaceMenuItem.Text = "&Replace";
			// 
			// buildMenu
			// 
			this.buildMenu.Name = "buildMenu";
			this.buildMenu.Size = new System.Drawing.Size(46, 20);
			this.buildMenu.Text = "&Build";
			// 
			// helpMenu
			// 
			this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutMenuItem});
			this.helpMenu.Name = "helpMenu";
			this.helpMenu.Size = new System.Drawing.Size(44, 20);
			this.helpMenu.Text = "&Help";
			// 
			// aboutMenuItem
			// 
			this.aboutMenuItem.Name = "aboutMenuItem";
			this.aboutMenuItem.Size = new System.Drawing.Size(180, 22);
			this.aboutMenuItem.Text = "&About SchemaBrute";
			// 
			// toolbar
			// 
			this.toolbar.Dock = System.Windows.Forms.DockStyle.None;
			this.toolbar.Location = new System.Drawing.Point(0, 25);
			this.toolbar.Name = "toolbar";
			this.toolbar.Padding = new System.Windows.Forms.Padding(0);
			this.toolbar.Size = new System.Drawing.Size(111, 25);
			this.toolbar.TabIndex = 0;
			// 
			// startPageTab
			// 
			this.startPageTab.Controls.Add(this.startPageView);
			this.startPageTab.Location = new System.Drawing.Point(4, 22);
			this.startPageTab.Margin = new System.Windows.Forms.Padding(0);
			this.startPageTab.Name = "startPageTab";
			this.startPageTab.Size = new System.Drawing.Size(776, 467);
			this.startPageTab.TabIndex = 0;
			this.startPageTab.Text = "Start page";
			this.startPageTab.UseVisualStyleBackColor = true;
			// 
			// startPageView
			// 
			this.startPageView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.startPageView.Location = new System.Drawing.Point(0, 0);
			this.startPageView.Margin = new System.Windows.Forms.Padding(0);
			this.startPageView.Name = "startPageView";
			this.startPageView.ScriptErrorsSuppressed = true;
			this.startPageView.Size = new System.Drawing.Size(776, 467);
			this.startPageView.TabIndex = 0;
			this.startPageView.TabStop = false;
			// 
			// tabs
			// 
			this.tabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tabs.Controls.Add(this.startPageTab);
			this.tabs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabs.Location = new System.Drawing.Point(0, 49);
			this.tabs.Margin = new System.Windows.Forms.Padding(0);
			this.tabs.Name = "tabs";
			this.tabs.SelectedIndex = 0;
			this.tabs.ShowToolTips = true;
			this.tabs.Size = new System.Drawing.Size(784, 493);
			this.tabs.TabIndex = 0;
			this.tabs.TabStop = false;
			this.tabs.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabs_MouseClick);
			// 
			// tabContextMenu
			// 
			this.tabContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tabSaveMenuItem,
            this.tabCloseMenuItem,
            this.tabCloseAllMenuItem,
            this.tabMenuSeparator1,
            this.copToolStripMenuItem,
            this.yToolStripMenuItem,
            this.tabMenuSeparator2,
            this.openContainingFolderToolStripMenuItem});
			this.tabContextMenu.Name = "tabContextMenu";
			this.tabContextMenu.Size = new System.Drawing.Size(198, 148);
			// 
			// tabSaveMenuItem
			// 
			this.tabSaveMenuItem.Name = "tabSaveMenuItem";
			this.tabSaveMenuItem.Size = new System.Drawing.Size(197, 22);
			this.tabSaveMenuItem.Text = "&Save";
			// 
			// tabCloseMenuItem
			// 
			this.tabCloseMenuItem.Name = "tabCloseMenuItem";
			this.tabCloseMenuItem.Size = new System.Drawing.Size(197, 22);
			this.tabCloseMenuItem.Text = "&Close this tab";
			// 
			// tabCloseAllMenuItem
			// 
			this.tabCloseAllMenuItem.Name = "tabCloseAllMenuItem";
			this.tabCloseAllMenuItem.Size = new System.Drawing.Size(197, 22);
			this.tabCloseAllMenuItem.Text = "Close &all other tabs";
			// 
			// tabMenuSeparator1
			// 
			this.tabMenuSeparator1.Name = "tabMenuSeparator1";
			this.tabMenuSeparator1.Size = new System.Drawing.Size(194, 6);
			// 
			// copToolStripMenuItem
			// 
			this.copToolStripMenuItem.Name = "copToolStripMenuItem";
			this.copToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
			this.copToolStripMenuItem.Text = "Copy &file name";
			// 
			// yToolStripMenuItem
			// 
			this.yToolStripMenuItem.Name = "yToolStripMenuItem";
			this.yToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
			this.yToolStripMenuItem.Text = "Copy full &path";
			// 
			// tabMenuSeparator2
			// 
			this.tabMenuSeparator2.Name = "tabMenuSeparator2";
			this.tabMenuSeparator2.Size = new System.Drawing.Size(194, 6);
			// 
			// openContainingFolderToolStripMenuItem
			// 
			this.openContainingFolderToolStripMenuItem.Name = "openContainingFolderToolStripMenuItem";
			this.openContainingFolderToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
			this.openContainingFolderToolStripMenuItem.Text = "&Open containing folder";
			// 
			// IDE
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 564);
			this.Controls.Add(this.tabs);
			this.Controls.Add(this.toolbar);
			this.Controls.Add(this.statusbar);
			this.Controls.Add(this.menubar);
			this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MainMenuStrip = this.menubar;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "IDE";
			this.Text = "SchemaBrute";
			this.menubar.ResumeLayout(false);
			this.menubar.PerformLayout();
			this.startPageTab.ResumeLayout(false);
			this.tabs.ResumeLayout(false);
			this.tabContextMenu.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.StatusStrip statusbar;
		private System.Windows.Forms.MenuStrip menubar;
		private System.Windows.Forms.ToolStripMenuItem fileMenu;
		private System.Windows.Forms.ToolStripMenuItem editMenu;
		private System.Windows.Forms.ToolStripMenuItem buildMenu;
		private System.Windows.Forms.ToolStripMenuItem helpMenu;
		private System.Windows.Forms.ToolStrip toolbar;
		private System.Windows.Forms.TabPage startPageTab;
		private System.Windows.Forms.TabControl tabs;
		private System.Windows.Forms.WebBrowser startPageView;
		private System.Windows.Forms.ToolStripMenuItem fileNewMenu;
		private System.Windows.Forms.ToolStripMenuItem newProjectMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newSchemaMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newFileMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fileOpenMenu;
		private System.Windows.Forms.ToolStripMenuItem openProjectMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openSchemaMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openFileMenuItem;
		private System.Windows.Forms.ToolStripSeparator fileMenuSeparator1;
		private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsMenuItem;
		private System.Windows.Forms.ToolStripSeparator fileMenuSeparator2;
		private System.Windows.Forms.ToolStripMenuItem fileCloseMenu;
		private System.Windows.Forms.ToolStripSeparator fileMenuSeparator3;
		private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
		private System.Windows.Forms.ToolStripMenuItem closeFileMenuItem;
		private System.Windows.Forms.ToolStripMenuItem closeProjectMenuItem;
		private System.Windows.Forms.ToolStripMenuItem undoMenuItem;
		private System.Windows.Forms.ToolStripMenuItem redoMenuItem;
		private System.Windows.Forms.ToolStripSeparator editMenuSeparator1;
		private System.Windows.Forms.ToolStripMenuItem copyMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cutMenuItem;
		private System.Windows.Forms.ToolStripMenuItem pasteMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
		private System.Windows.Forms.ToolStripSeparator editMenuSeparator2;
		private System.Windows.Forms.ToolStripMenuItem selectAllMenuItem;
		private System.Windows.Forms.ToolStripSeparator editMenuSeparator3;
		private System.Windows.Forms.ToolStripMenuItem findReplaceMenu;
		private System.Windows.Forms.ToolStripMenuItem findMenuItem;
		private System.Windows.Forms.ToolStripMenuItem replaceMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAllMenuItem;
		private System.Windows.Forms.ContextMenuStrip tabContextMenu;
		private System.Windows.Forms.ToolStripMenuItem tabSaveMenuItem;
		private System.Windows.Forms.ToolStripMenuItem tabCloseMenuItem;
		private System.Windows.Forms.ToolStripMenuItem tabCloseAllMenuItem;
		private System.Windows.Forms.ToolStripSeparator tabMenuSeparator1;
		private System.Windows.Forms.ToolStripMenuItem copToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem yToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator tabMenuSeparator2;
		private System.Windows.Forms.ToolStripMenuItem openContainingFolderToolStripMenuItem;
	}
}

