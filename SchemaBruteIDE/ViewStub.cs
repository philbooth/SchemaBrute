// Copyright � 2009 Phil Booth
//
// This file is part of SchemaBruteIDE.
//
// SchemaBruteIDE is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// SchemaBruteIDE is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SchemaBruteIDE. If not, see <http://www.gnu.org/licenses/>.

using System;

namespace SchemaBruteIDE
{
	class ViewStub : View
	{
		string View.SelectSchema()
		{
			selectSchemaCalled = true;
			return schemaPath;
		}

		void View.LoadSchema(string schemaPath, byte[] schema)
		{
			loadSchemaCalled = true;
		}

		public bool SelectSchemaCalled
		{
			get
			{
				return selectSchemaCalled;
			}
		}

		public bool LoadSchemaCalled
		{
			get
			{
				return loadSchemaCalled;
			}
		}

		public void ClearSchemaPath()
		{
			schemaPath = "";
		}

		private string schemaPath = TestData.Path + "xmlschema.xsd";
		private bool selectSchemaCalled = false;
		private bool loadSchemaCalled = false;
	}
}
