// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;

namespace LibSchemaBrute
{
	class Counts
	{
		public enum Index
		{
			Element,
			Sequence,
			Choice,
			All,
			Group,
			SimpleType,
			SimpleTypeRestriction,
			Length,
			MinLength,
			MaxLength,
			Pattern,
			Enumeration,
			MinInclusive,
			MaxInclusive,
			MinExclusive,
			MaxExclusive,
			TotalDigits,
			FractionDigits,
			WhiteSpace,
			SimpleTypeUnion,
			SimpleTypeList,
			ComplexType,
			Attribute,
			Any,
			AnyAttribute,
			SimpleContentExtension,
			SimpleContentRestriction,
			ComplexContentExtension,
			ComplexContentRestriction
		};

		public Counts()
		{
			Reset();
		}

		public void Reset()
		{
			for(int i = 0; i < counts.Length; ++i)
				counts[i] = 0;
		}

		public void Increment(Index index)
		{
			++counts[(int)index];
		}

		public void Set(Index index, int count)
		{
			counts[(int)index] = count;
		}

		public int Get(Index index)
		{
			return counts[(int)index];
		}

		public int Length
		{
			get
			{
				return counts.Length;
			}
		}

		private int[] counts = new int[(int)Index.ComplexContentRestriction + 1];
	}
}
