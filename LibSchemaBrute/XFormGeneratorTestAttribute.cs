// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Xml;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class XFormGeneratorTestAttribute : XFormGeneratorTest
	{
		[SetUp]
		public void SetUp()
		{
			Initialise();
			HandleElementAttribute("foo", "bar");
		}

		[Test]
		public void TestAttributeData()
		{
			Assert.AreEqual(1, data.SelectNodes("*").Count, "Wrong number of data nodes");
			Assert.AreEqual("foo", data.SelectSingleNode("*").Name, "Bad datum [name]");
			Assert.AreEqual(1, data.SelectSingleNode("foo").Attributes.Count, "Wrong number of data nodes");
			Assert.AreEqual("bar", data.SelectSingleNode("foo/@*").Name, "Bad datum [name]");
		}

		[Test]
		public void TestAttributeLabel()
		{
			Assert.AreEqual(1, labels.SelectNodes("*").Count, "Wrong number of label nodes");
			Assert.AreEqual("elem_foo", labels.SelectSingleNode("*").Name, "Bad label datum [name]");
			Assert.AreEqual("foo", labels.SelectSingleNode("elem_foo/@label").Value, "Bad label datum [value]");
			Assert.AreEqual(1, labels.SelectNodes("elem_foo/*").Count, "Wrong number of data nodes");
			Assert.AreEqual("attr_bar", labels.SelectSingleNode("elem_foo/*").Name, "Bad label datum [name]");
			Assert.AreEqual("bar", labels.SelectSingleNode("elem_foo/attr_bar/@label").Value, "Bad label datum [value]");
		}

		[Test]
		public void TestAttributeBind()
		{
			Assert.AreEqual(1, binds.SelectNodes("*").Count, "Wrong number of binds");
			Assert.AreEqual("xf:bind", binds.SelectSingleNode("*").Name, "Bad bind [name]");
			Assert.AreEqual("foo", binds.SelectSingleNode("xf:bind/@nodeset", namespaceManager).Value, "Bad bind [nodeset]");
			Assert.AreEqual(1, binds.SelectNodes("xf:bind/*", namespaceManager).Count, "Wrong number of binds");
			Assert.AreEqual("xf:bind", binds.SelectSingleNode("xf:bind/*", namespaceManager).Name, "Bad bind [name]");
			Assert.AreEqual("@bar", binds.SelectSingleNode("xf:bind/xf:bind/@nodeset", namespaceManager).Value, "Bad bind [nodeset]");
		}

		[Test]
		public void TestAttributeUi()
		{
			Assert.AreEqual(1, ui.SelectNodes("xf:group", namespaceManager).Count, "Wrong number of groups");
			Assert.AreEqual(0, ui.SelectNodes("xf:input", namespaceManager).Count, "Wrong number of inputs");
			Assert.AreEqual(0, ui.SelectNodes("xf:trigger", namespaceManager).Count, "Wrong number of triggers");
			Assert.AreEqual("foo", ui.SelectSingleNode("xf:group/@ref", namespaceManager).Value, "Bad input [ref]");
			Assert.AreEqual(1, ui.SelectNodes("xf:group/xf:label", namespaceManager).Count, "Wrong number of labels");
			Assert.AreEqual("instance('instLabels')/elem_foo", ui.SelectSingleNode("xf:group/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
			Assert.AreEqual(0, ui.SelectNodes("xf:group/xf:group", namespaceManager).Count, "Wrong number of groups");
			Assert.AreEqual(1, ui.SelectNodes("xf:group/xf:input", namespaceManager).Count, "Wrong number of inputs");
			Assert.AreEqual(0, ui.SelectNodes("xf:group/xf:trigger", namespaceManager).Count, "Wrong number of triggers");
			Assert.AreEqual("@bar", ui.SelectSingleNode("xf:group/xf:input/@ref", namespaceManager).Value, "Bad input [ref]");
			Assert.AreEqual(1, ui.SelectNodes("xf:group/xf:input/xf:label", namespaceManager).Count, "Wrong number of labels");
			Assert.AreEqual("instance('instLabels')/elem_foo/attr_bar", ui.SelectSingleNode("xf:group/xf:input/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
		}

		[Test]
		public void TestAttributeTemplates()
		{
			Assert.AreEqual(1, templates.SelectNodes("*").Count, "Wrong number of templates");
			Assert.AreEqual("foo", templates.SelectSingleNode("*").Name, "Bad template [name]");
			Assert.AreEqual(1, templates.SelectSingleNode("foo").Attributes.Count, "Wrong number of templates");
			Assert.AreEqual("bar", templates.SelectSingleNode("foo/@*").Name, "Bad template [name]");
		}
	}
}
