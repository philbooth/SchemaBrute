// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class CountsTest
	{
		[SetUp]
		public void SetUp()
		{
			counts = new Counts();
		}

		[Test]
		public void IncrementElementCount()
		{
			VerifyIncrement(Counts.Index.Element, "element");
		}

		[Test]
		public void SetElementCount()
		{
			VerifySet(Counts.Index.Element, "element");
		}

		[Test]
		public void IncrementSequenceCount()
		{
			VerifyIncrement(Counts.Index.Sequence, "sequence");
		}

		[Test]
		public void SetSequenceCount()
		{
			VerifySet(Counts.Index.Sequence, "sequence");
		}

		[Test]
		public void IncrementChoiceCount()
		{
			VerifyIncrement(Counts.Index.Choice, "choice");
		}

		[Test]
		public void SetChoiceCount()
		{
			VerifySet(Counts.Index.Choice, "choice");
		}

		[Test]
		public void IncrementAllCount()
		{
			VerifyIncrement(Counts.Index.All, "all");
		}

		[Test]
		public void SetAllCount()
		{
			VerifySet(Counts.Index.All, "all");
		}

		[Test]
		public void IncrementGroupCount()
		{
			VerifyIncrement(Counts.Index.Group, "group");
		}

		[Test]
		public void SetGroupCount()
		{
			VerifySet(Counts.Index.Group, "group");
		}

		[Test]
		public void IncrementSimpleTypeCount()
		{
			VerifyIncrement(Counts.Index.SimpleType, "simple type");
		}

		[Test]
		public void SetSimpleTypeCount()
		{
			VerifySet(Counts.Index.SimpleType, "simple type");
		}

		[Test]
		public void IncrementSimpleTypeRestrictionCount()
		{
			VerifyIncrement(Counts.Index.SimpleTypeRestriction, "simple type restriction");
		}

		[Test]
		public void SetSimpleTypeRestrictionCount()
		{
			VerifySet(Counts.Index.SimpleTypeRestriction, "simple type restriction");
		}

		[Test]
		public void IncrementLengthCount()
		{
			VerifyIncrement(Counts.Index.Length, "length");
		}

		[Test]
		public void SetLengthCount()
		{
			VerifySet(Counts.Index.Length, "length");
		}

		[Test]
		public void IncrementMinLengthCount()
		{
			VerifyIncrement(Counts.Index.MinLength, "minimum length");
		}

		[Test]
		public void SetMinLengthCount()
		{
			VerifySet(Counts.Index.MinLength, "minimum length");
		}

		[Test]
		public void IncrementMaxLengthCount()
		{
			VerifyIncrement(Counts.Index.MaxLength, "maximum length");
		}

		[Test]
		public void SetMaxLengthCount()
		{
			VerifySet(Counts.Index.MaxLength, "maximum length");
		}

		[Test]
		public void IncrementPatternCount()
		{
			VerifyIncrement(Counts.Index.Pattern, "pattern");
		}

		[Test]
		public void SetPatternCount()
		{
			VerifySet(Counts.Index.Pattern, "pattern");
		}

		[Test]
		public void IncrementEnumerationCount()
		{
			VerifyIncrement(Counts.Index.Enumeration, "enumeration");
		}

		[Test]
		public void SetEnumerationCount()
		{
			VerifySet(Counts.Index.Enumeration, "enumeration");
		}

		[Test]
		public void IncrementMinInclusiveCount()
		{
			VerifyIncrement(Counts.Index.MinInclusive, "minimum inclusive");
		}

		[Test]
		public void SetMinInclusiveCount()
		{
			VerifySet(Counts.Index.MinInclusive, "minimum inclusive");
		}

		[Test]
		public void IncrementMaxInclusiveCount()
		{
			VerifyIncrement(Counts.Index.MaxInclusive, "maximum inclusive");
		}

		[Test]
		public void SetMaxInclusiveCount()
		{
			VerifySet(Counts.Index.MaxInclusive, "maximum inclusive");
		}

		[Test]
		public void IncrementMinExclusiveCount()
		{
			VerifyIncrement(Counts.Index.MinExclusive, "minimum exclusive");
		}

		[Test]
		public void SetMinExclusiveCount()
		{
			VerifySet(Counts.Index.MinExclusive, "minimum exclusive");
		}

		[Test]
		public void IncrementMaxExclusiveCount()
		{
			VerifyIncrement(Counts.Index.MaxExclusive, "maximum exclusive");
		}

		[Test]
		public void SetMaxExclusiveCount()
		{
			VerifySet(Counts.Index.MaxExclusive, "maximum exclusive");
		}

		[Test]
		public void IncrementTotalDigitsCount()
		{
			VerifyIncrement(Counts.Index.TotalDigits, "total digits");
		}

		[Test]
		public void SetTotalDigitsCount()
		{
			VerifySet(Counts.Index.TotalDigits, "total digits");
		}

		[Test]
		public void IncrementFractionDigitsCount()
		{
			VerifyIncrement(Counts.Index.FractionDigits, "fraction digits");
		}

		[Test]
		public void SetFractionDigitsCount()
		{
			VerifySet(Counts.Index.FractionDigits, "fraction digits");
		}

		[Test]
		public void IncrementWhiteSpaceCount()
		{
			VerifyIncrement(Counts.Index.WhiteSpace, "white space");
		}

		[Test]
		public void SetWhiteSpaceCount()
		{
			VerifySet(Counts.Index.WhiteSpace, "white space");
		}

		[Test]
		public void IncrementSimpleTypeUnionCount()
		{
			VerifyIncrement(Counts.Index.SimpleTypeUnion, "simple type union");
		}

		[Test]
		public void SetSimpleTypeUnionCount()
		{
			VerifySet(Counts.Index.SimpleTypeUnion, "simple type union");
		}

		[Test]
		public void IncrementSimpleTypeListCount()
		{
			VerifyIncrement(Counts.Index.SimpleTypeList, "simple type list");
		}

		[Test]
		public void SetSimpleTypeListCount()
		{
			VerifySet(Counts.Index.SimpleTypeList, "simple type list");
		}

		[Test]
		public void IncrementComplexTypeCount()
		{
			VerifyIncrement(Counts.Index.ComplexType, "complex type");
		}

		[Test]
		public void SetComplexTypeCount()
		{
			VerifySet(Counts.Index.ComplexType, "complex type");
		}

		[Test]
		public void IncrementAttributeCount()
		{
			VerifyIncrement(Counts.Index.Attribute, "attribute");
		}

		[Test]
		public void SetAttributeCount()
		{
			VerifySet(Counts.Index.Attribute, "attribute");
		}

		[Test]
		public void IncrementAnyCount()
		{
			VerifyIncrement(Counts.Index.Any, "any");
		}

		[Test]
		public void SetAnyCount()
		{
			VerifySet(Counts.Index.Any, "any");
		}

		[Test]
		public void IncrementAnyAttributeCount()
		{
			VerifyIncrement(Counts.Index.AnyAttribute, "anyAttribute");
		}

		[Test]
		public void SetAnyAttributeCount()
		{
			VerifySet(Counts.Index.AnyAttribute, "anyAttribute");
		}

		[Test]
		public void IncrementSimpleContentExtensionCount()
		{
			VerifyIncrement(Counts.Index.SimpleContentExtension, "simple content extension");
		}

		[Test]
		public void SetSimpleContentExtensionCount()
		{
			VerifySet(Counts.Index.SimpleContentExtension, "simple content extension");
		}

		[Test]
		public void IncrementSimpleContentRestrictionCount()
		{
			VerifyIncrement(Counts.Index.SimpleContentRestriction, "simple content restriction");
		}

		[Test]
		public void SetSimpleContentRestrictionCount()
		{
			VerifySet(Counts.Index.SimpleContentRestriction, "simple content restriction");
		}

		[Test]
		public void IncrementComplexContentExtensionCount()
		{
			VerifyIncrement(Counts.Index.ComplexContentExtension, "complex content extension");
		}

		[Test]
		public void SetComplexContentExtensionCount()
		{
			VerifySet(Counts.Index.ComplexContentExtension, "complex content extension");
		}

		[Test]
		public void IncrementComplexContentRestrictionCount()
		{
			VerifyIncrement(Counts.Index.ComplexContentRestriction, "complex content restriction");
		}

		[Test]
		public void SetComplexContentRestrictionCount()
		{
			VerifySet(Counts.Index.ComplexContentRestriction, "complex content restriction");
		}

		private void VerifyIncrement(Counts.Index index, string countName)
		{
			VerifyTargetCountIncrement(index, countName);
			VerifyRemainingCounts(index);
		}

		private void VerifyTargetCountIncrement(Counts.Index index, string countName)
		{
			Assert.AreEqual(0, counts.Get(index), "Incorrectly initialised " + countName + " count");
			counts.Increment(index);
			Assert.AreEqual(1, counts.Get(index), "Increment() resulted in incorrect " + countName + " count");
			counts.Increment(index);
			Assert.AreEqual(2, counts.Get(index), "Increment() resulted in incorrect " + countName + " count");
		}

		private void VerifySet(Counts.Index index, string countName)
		{
			VerifyTargetCountSet(index, countName);
			VerifyRemainingCounts(index);
		}

		private void VerifyTargetCountSet(Counts.Index index, string countName)
		{
			Assert.AreEqual(0, counts.Get(index), "Incorrectly initialised " + countName + " count");
			counts.Set(index, 1);
			Assert.AreEqual(1, counts.Get(index), "Set() resulted in incorrect " + countName + " count");
			counts.Set(index, 2);
			Assert.AreEqual(2, counts.Get(index), "Set() resulted in incorrect " + countName + " count");
			counts.Set(index, 100);
			Assert.AreEqual(100, counts.Get(index), "Set() resulted in incorrect " + countName + " count");
		}

		private void VerifyRemainingCounts(Counts.Index index)
		{
			for(int i = 0; i < counts.Length; ++i)
			{
				if(i != (int)index)
					Assert.AreEqual(0, counts.Get((Counts.Index)i), "Unexepected non-zero count at index " + i);
			}
		}

		private Counts counts;
	}
}
