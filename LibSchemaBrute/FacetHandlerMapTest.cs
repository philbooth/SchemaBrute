// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Xml.Schema;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class FacetHandlerMapTest
	{
		[SetUp]
		public void SetUp()
		{
			handler = new HandlerStub();
			expectedCounts.Reset();
		}

		[Test]
		public void HandleLength()
		{
			FacetHandlerMap.Handle(new XmlSchemaLengthFacet(), handler);
			expectedCounts.Set(Counts.Index.Length, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleMinLength()
		{
			FacetHandlerMap.Handle(new XmlSchemaMinLengthFacet(), handler);
			expectedCounts.Set(Counts.Index.MinLength, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleMaxLength()
		{
			FacetHandlerMap.Handle(new XmlSchemaMaxLengthFacet(), handler);
			expectedCounts.Set(Counts.Index.MaxLength, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandlePattern()
		{
			FacetHandlerMap.Handle(new XmlSchemaPatternFacet(), handler);
			expectedCounts.Set(Counts.Index.Pattern, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleEnumeration()
		{
			FacetHandlerMap.Handle(new XmlSchemaEnumerationFacet(), handler);
			expectedCounts.Set(Counts.Index.Enumeration, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleMinInclusive()
		{
			FacetHandlerMap.Handle(new XmlSchemaMinInclusiveFacet(), handler);
			expectedCounts.Set(Counts.Index.MinInclusive, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleMaxInclusive()
		{
			FacetHandlerMap.Handle(new XmlSchemaMaxInclusiveFacet(), handler);
			expectedCounts.Set(Counts.Index.MaxInclusive, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleMinExclusive()
		{
			FacetHandlerMap.Handle(new XmlSchemaMinExclusiveFacet(), handler);
			expectedCounts.Set(Counts.Index.MinExclusive, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleMaxExclusive()
		{
			FacetHandlerMap.Handle(new XmlSchemaMaxExclusiveFacet(), handler);
			expectedCounts.Set(Counts.Index.MaxExclusive, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleTotalDigits()
		{
			FacetHandlerMap.Handle(new XmlSchemaTotalDigitsFacet(), handler);
			expectedCounts.Set(Counts.Index.TotalDigits, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleFractionDigits()
		{
			FacetHandlerMap.Handle(new XmlSchemaFractionDigitsFacet(), handler);
			expectedCounts.Set(Counts.Index.FractionDigits, 1);
			handler.AssertCounts(expectedCounts);
		}

		[Test]
		public void HandleWhiteSpace()
		{
			FacetHandlerMap.Handle(new XmlSchemaWhiteSpaceFacet(), handler);
			expectedCounts.Set(Counts.Index.WhiteSpace, 1);
			handler.AssertCounts(expectedCounts);
		}

		private HandlerStub handler;
		private Counts expectedCounts = new Counts();
	}
}
