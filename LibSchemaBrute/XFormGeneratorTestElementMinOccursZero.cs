// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Xml;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class XFormGeneratorTestElementMinOccursZero : XFormGeneratorTest
	{
		[SetUp]
		public void SetUp()
		{
			Initialise();
			HandleElement("foo", 0);
		}

		[Test]
		public void TestElementMinOccursZero()
		{
			Assert.AreEqual(1, ui.SelectNodes("xf:trigger", namespaceManager).Count, "Wrong number of delete triggers");
			Assert.AreEqual("instance('instLabels')/elem_foo/delete", ui.SelectSingleNode("xf:trigger/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
			Assert.IsNotNull(labels.SelectSingleNode("elem_foo/delete"), "Bad label datum");
			Assert.AreEqual("Delete foo", labels.SelectSingleNode("elem_foo/delete/@label").Value, "Bad label datum [value]");
		}

		[Test]
		public void TestDeleteTriggerContainsAction()
		{
			Assert.AreEqual(1, ui.SelectNodes("xf:trigger/xf:action", namespaceManager).Count, "Wrong number of actions");
		}

		[Test]
		public void TestDeleteActionEvent()
		{
			Assert.AreEqual("DOMActivate", ui.SelectSingleNode("xf:trigger/xf:action/@ev:event", namespaceManager).Value, "Wrong event");
		}

		[Test]
		public void TestDeleteActionContainsDelete()
		{
			Assert.AreEqual(1, ui.SelectNodes("xf:trigger/xf:action/xf:delete", namespaceManager).Count, "Wrong number of action children");
		}

		[Test]
		public void TestDeleteActionContextAttribute()
		{
			Assert.AreEqual(".", ui.SelectSingleNode("xf:trigger/xf:action/xf:delete/@context", namespaceManager).Value, "Wrong context");
		}

		[Test]
		public void TestDeleteActionNodesetAttribute()
		{
			Assert.AreEqual("foo", ui.SelectSingleNode("xf:trigger/xf:action/xf:delete/@nodeset", namespaceManager).Value, "Wrong context");
		}

		[Test]
		public void TestDeleteStateDatum()
		{
			Assert.AreEqual(1, state.SelectNodes("*").Count, "Wrong number of state data nodes");
		}

		[Test]
		public void TestDeleteStateDatumName()
		{
			Assert.AreEqual("delete-foo", state.SelectSingleNode("*").Name, "Wrong node name");
		}

		// TODO: xf:bind for state instance (trigger relevancy etc)
	}
}
