// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;

namespace LibSchemaBrute
{
	class XFormGenerator : Handler
	{
		// TODO: Eliminate magic strings

		void Handler.BeginElement(XmlSchemaElement element)
		{
			instanceHandler.BeginElement(element);
			templateHandler.BeginElement(element);

			CreateParentGroup();
			CreateElementLabelDatum(element.Name);
			CreateElementBind(element.Name);

			currentLabelXPath += "/elem_" + element.Name;

			ContextElement = element;

			elementIsGroup = false;
		}

		private void CreateElementLabelDatum(string label)
		{
			currentLabelsNode = CreateLabelDatum("elem_" + label, label);
		}

		private XmlElement CreateLabelDatum(string name, string label)
		{
			XmlElement labelElement = xform.CreateElement(name);
			labelElement.SetAttribute("label", label);
			currentLabelsNode.AppendChild(labelElement);

			return labelElement;
		}

		private void CreateElementBind(string nodeset)
		{
			currentBindsNode = CreateBind(nodeset);
		}

		private XmlElement CreateBind(string nodeset)
		{
			XmlElement bindElement = CreateXFormsElement("bind");
			bindElement.SetAttribute("nodeset", nodeset);
			currentBindsNode.AppendChild(bindElement);

			return bindElement;
		}

		private XmlElement CreateXFormsElement(string name)
		{
			return xform.CreateElement(xformsNamespacePrefix, name, xformsNamespace);
		}

		private void CreateParentGroup()
		{
			if(schemaContext.Count > 0)
				CreateGroup();
		}

		private XmlSchemaElement ContextElement
		{
			get
			{
				return ((XmlSchemaElement)schemaContext.Peek());
			}

			set
			{
				schemaContext.Push(value);
			}
		}

		private void CreateGroup()
		{
			XmlElement groupElement = CreateXFormsElement("group");
			CreateRef(groupElement, ContextElement.Name);
			currentUINode.AppendChild(groupElement);
			CreateLabel(groupElement);

			if(ContextElement.MinOccurs == 0)
				CreateDeleteTrigger(ContextElement.Name);

			currentUINode = groupElement;
		}

		private void CreateRef(XmlElement element, string value)
		{
			element.SetAttribute("ref", value);
		}

		private void CreateLabel(XmlElement parent)
		{
			XmlElement label = xform.CreateElement(xformsNamespacePrefix, "label", xformsNamespace);
			CreateRef(label, currentLabelXPath);
			parent.AppendChild(label);
		}

		private void CreateDeleteTrigger(string name)
		{
			CreateDeleteAction(name, "DOMActivate", CreateTrigger("delete", "Delete " + name));

			XmlElement stateElement = xform.CreateElement("delete-" + name);
			currentStateNode.AppendChild(stateElement);
		}

		private XmlElement CreateTrigger(string labelName, string label)
		{
			XmlElement triggerElement = CreateXFormsElement("trigger");
			currentUINode.AppendChild(triggerElement);

			CreateNamedLabel(labelName, label, triggerElement);

			return triggerElement;
		}

		private void CreateNamedLabel(string labelName, string label, XmlElement parentElement)
		{
			currentLabelsNode = CreateLabelDatum(labelName, label);

			currentLabelXPath += "/" + labelName;
			CreateLabel(parentElement);
			currentLabelXPath = currentLabelXPath.Remove(currentLabelXPath.LastIndexOf('/'));

			currentLabelsNode = currentLabelsNode.ParentNode;
		}

		private void CreateDeleteAction(string nodeset, string eventType, XmlElement parentElement)
		{
			XmlElement actionElement = CreateAction(eventType, parentElement);

			XmlElement deleteElement = CreateXFormsElement("delete");
			deleteElement.SetAttribute("context", ".");
			deleteElement.SetAttribute("nodeset", nodeset);
			actionElement.AppendChild(deleteElement);
		}

		private XmlElement CreateAction(string eventType, XmlElement parentElement)
		{
			XmlElement actionElement = CreateXFormsElement("action");
			actionElement.SetAttribute("event", eventsNamespace, eventType);
			parentElement.AppendChild(actionElement);

			return actionElement;
		}

		void Handler.EndElement()
		{
			if(elementIsGroup)
				currentUINode = currentUINode.ParentNode;
			else
			{
				CreateElementInput(ContextElement);
				elementIsGroup = true;
			}

			schemaContext.Pop();
			currentLabelXPath = currentLabelXPath.Remove(currentLabelXPath.LastIndexOf('/'));
			currentBindsNode = currentBindsNode.ParentNode;
			currentLabelsNode = currentLabelsNode.ParentNode;
			templateHandler.EndElement();
			instanceHandler.EndElement();
		}

		private void CreateElementInput(XmlSchemaElement schemaElement)
		{
			XmlElement inputElement = CreateXFormsElement("input");
			CreateRef(inputElement, schemaElement.Name);
			currentUINode.AppendChild(inputElement);
			CreateLabel(inputElement);
			if(schemaElement.MinOccurs == 0)
				CreateDeleteTrigger(schemaElement.Name);
		}

		void Handler.BeginSequence(XmlSchemaSequence sequence)
		{
		}

		void Handler.EndSequence()
		{
		}

		void Handler.BeginChoice(XmlSchemaChoice choice)
		{
		}

		void Handler.EndChoice()
		{
		}

		void Handler.BeginAll(XmlSchemaAll all)
		{
		}

		void Handler.EndAll()
		{
		}

		void Handler.BeginGroup(XmlSchemaGroup group)
		{
		}

		void Handler.EndGroup()
		{
		}

		void Handler.BeginSimpleType(XmlSchemaSimpleType simpleType)
		{
		}

		void Handler.EndSimpleType()
		{
		}

		void Handler.BeginSimpleTypeRestriction(XmlSchemaSimpleTypeRestriction simpleTypeRestriction)
		{
		}

		void Handler.EndSimpleTypeRestriction()
		{
		}

		void Handler.Length(XmlSchemaFacet length)
		{
		}

		void Handler.MinLength(XmlSchemaFacet minLength)
		{
		}

		void Handler.MaxLength(XmlSchemaFacet maxLength)
		{
		}

		void Handler.Pattern(XmlSchemaFacet pattern)
		{
		}

		void Handler.Enumeration(XmlSchemaFacet enumeration)
		{
		}

		void Handler.MinInclusive(XmlSchemaFacet minInclusive)
		{
		}

		void Handler.MaxInclusive(XmlSchemaFacet maxInclusive)
		{
		}

		void Handler.MinExclusive(XmlSchemaFacet minExclusive)
		{
		}

		void Handler.MaxExclusive(XmlSchemaFacet maxExclusive)
		{
		}

		void Handler.TotalDigits(XmlSchemaFacet totalDigits)
		{
		}

		void Handler.FractionDigits(XmlSchemaFacet fractionDigits)
		{
		}

		void Handler.WhiteSpace(XmlSchemaFacet whiteSpace)
		{
		}

		void Handler.BeginSimpleTypeUnion(XmlSchemaSimpleTypeUnion union)
		{
		}

		void Handler.EndSimpleTypeUnion()
		{
		}

		void Handler.BeginSimpleTypeList(XmlSchemaSimpleTypeList list)
		{
		}

		void Handler.EndSimpleTypeList()
		{
		}

		void Handler.BeginComplexType(XmlSchemaComplexType complexType)
		{
		}

		void Handler.EndComplexType()
		{
		}

		void Handler.BeginAttribute(XmlSchemaAttribute attribute)
		{
			instanceHandler.BeginAttribute(attribute);
			templateHandler.BeginAttribute(attribute);
			CreateAttributeLabelDatum(attribute.Name);
			CreateAttributeBind(attribute.Name);
			CreateParentGroup();

			currentLabelXPath += "/attr_" + attribute.Name;

			CreateAttributeInput(attribute.Name);
			elementIsGroup = true;
		}

		private void CreateAttributeLabelDatum(string label)
		{
			CreateLabelDatum("attr_" + label, label);
		}

		private void CreateAttributeBind(string attributeName)
		{
			CreateBind("@" + attributeName);
		}

		private void CreateAttributeInput(string refAttribute)
		{
			XmlElement inputElement = CreateXFormsElement("input");
			CreateRef(inputElement, "@" + refAttribute);
			currentUINode.AppendChild(inputElement);
			CreateLabel(inputElement);
		}

		void Handler.EndAttribute()
		{
			currentLabelXPath = currentLabelXPath.Remove(currentLabelXPath.LastIndexOf('/'));
			templateHandler.EndAttribute();
			instanceHandler.EndAttribute();
		}

		void Handler.BeginAny(XmlSchemaAny any)
		{
		}

		void Handler.EndAny()
		{
		}

		void Handler.BeginAnyAttribute(XmlSchemaAnyAttribute anyAttribute)
		{
		}

		void Handler.EndAnyAttribute()
		{
		}

		void Handler.BeginSimpleContentExtension(XmlSchemaSimpleContentExtension simpleContentExtension)
		{
		}

		void Handler.EndSimpleContentExtension()
		{
		}

		void Handler.BeginSimpleContentRestriction(XmlSchemaSimpleContentRestriction simpleContentRestriction)
		{
		}

		void Handler.EndSimpleContentRestriction()
		{
		}

		void Handler.BeginComplexContentExtension(XmlSchemaComplexContentExtension complexContentExtension)
		{
		}

		void Handler.EndComplexContentExtension()
		{
		}

		void Handler.BeginComplexContentRestriction(XmlSchemaComplexContentRestriction complexContentRestriction)
		{
		}

		void Handler.EndComplexContentRestriction()
		{
		}

		public XFormGenerator(XmlDocument skeleton, XmlNamespaceManager namespaceManager, XmlNode dataRoot, XmlNode templatesRoot, XmlNode labelsRoot, XmlNode stateRoot, XmlNode bindsRoot, XmlNode uiRoot)
		{
			xform = skeleton;

			xformsNamespacePrefix = namespaceManager.LookupPrefix(xformsNamespace);
			if(xformsNamespacePrefix == "")
			{
				xformsNamespacePrefix = "xforms";
				namespaceManager.AddNamespace(xformsNamespacePrefix, xformsNamespace);
			}

			instanceGenerator = new InstanceGenerator(xform, dataRoot);
			instanceHandler = (Handler)instanceGenerator;
			templateGenerator = new InstanceGenerator(xform, templatesRoot);
			templateHandler = (Handler)templateGenerator;

			currentLabelsNode = labelsRoot;
			currentStateNode = stateRoot;
			currentBindsNode = bindsRoot;
			currentUINode = uiRoot;

			InitialiseCurrentLabelXPath();
		}

		private void InitialiseCurrentLabelXPath()
		{
			XmlNodeList ancestors = currentLabelsNode.SelectNodes("ancestor::*");
			foreach(XmlNode ancestor in ancestors)
			{
				if(ancestor.LocalName == "instance" && ancestor.NamespaceURI == xformsNamespace)
				{
					XmlNode instanceId = ancestor.SelectSingleNode("@id");
					if(instanceId != null && instanceId.Value != "")
						currentLabelXPath = "instance('" + instanceId.Value + "')";
					break;
				}
			}

			// TODO: Handle empty label instance (by creating a document element)?
		}

		private XFormGenerator()
		{
		}

		public XmlDocument Result
		{
			get
			{
				return xform;
			}
		}

		private XmlDocument xform;
		private string xformsNamespacePrefix;
		private InstanceGenerator instanceGenerator;
		private Handler instanceHandler;
		private InstanceGenerator templateGenerator;
		private Handler templateHandler;
		private XmlNode currentLabelsNode;
		private XmlNode currentStateNode;
		private XmlNode currentBindsNode;
		private XmlNode currentUINode;
		private string currentLabelXPath;
		private Stack<XmlSchemaObject> schemaContext = new Stack<XmlSchemaObject>();
		private bool elementIsGroup;

		const string xformsNamespace = "http://www.w3.org/2002/xforms";
		const string eventsNamespace = "http://www.w3.org/2001/xml-events";
	}
}
