// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Xml;
using System.Xml.Schema;
using NUnit.Framework;

namespace LibSchemaBrute
{
	class XFormGeneratorTest
	{
		protected void Initialise()
		{
			LoadXForm();
			InitialiseNamespaceManager();
			SelectInsertionPoints();
			InitialiseGenerator();
		}

		private void LoadXForm()
		{
			xform = new XmlDocument();
			xform.LoadXml("<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xf=\"http://www.w3.org/2002/xforms\" xmlns:ev=\"http://www.w3.org/2001/xml-events\"><head><title>SchemaBrute unit tests</title></head><body><xf:model><xf:instance id=\"instData\"><data xmlns=\"\" /></xf:instance><xf:instance id=\"instTemplates\"><templates xmlns=\"\" /></xf:instance><xf:instance id=\"instLabels\"><labels xmlns=\"\" /></xf:instance><xf:instance id=\"instState\"><state xmlns=\"\" /></xf:instance><xf:bind nodeset=\"instance('instData')\" /></xf:model><xf:group ref=\"instance('instData')\"></xf:group></body></html>");
		}

		private void InitialiseNamespaceManager()
		{
			namespaceManager = new XmlNamespaceManager(xform.NameTable);
			namespaceManager.AddNamespace("xh", "http://www.w3.org/1999/xhtml");
			namespaceManager.AddNamespace("xf", "http://www.w3.org/2002/xforms");
			namespaceManager.AddNamespace("ev", "http://www.w3.org/2001/xml-events");
		}

		private void SelectInsertionPoints()
		{
			data = xform.SelectSingleNode("/xh:html/xh:body/xf:model/xf:instance[@id = 'instData']/data", namespaceManager);
			templates = xform.SelectSingleNode("/xh:html/xh:body/xf:model/xf:instance[@id = 'instTemplates']/templates", namespaceManager);
			labels = xform.SelectSingleNode("/xh:html/xh:body/xf:model/xf:instance[@id = 'instLabels']/labels", namespaceManager);
			state = xform.SelectSingleNode("/xh:html/xh:body/xf:model/xf:instance[@id = 'instState']/state", namespaceManager);
			binds = xform.SelectSingleNode("/xh:html/xh:body/xf:model/xf:bind", namespaceManager);
			ui = xform.SelectSingleNode("/xh:html/xh:body/xf:group", namespaceManager);
		}

		private void InitialiseGenerator()
		{
			generator = new XFormGenerator(xform, namespaceManager, data, templates, labels, state, binds, ui);
			handler = (Handler)generator;
		}

		protected void AssertGeneratorInheritsHandler()
		{
			Assert.IsInstanceOf<Handler>(generator);
		}

		protected void AssertEmptyResult()
		{
			Assert.IsInstanceOf<XmlDocument>(generator.Result);
			Assert.AreSame(xform, generator.Result);
		}

		protected void HandleElement(string name, int minOccurs)
		{
			BeginElement(name, minOccurs);
			EndElement();
		}

		private void BeginElement(string name, int minOccurs)
		{
			XmlSchemaElement element = new XmlSchemaElement();
			element.Name = name;
			element.MinOccurs = minOccurs;
			handler.BeginElement(element);
		}

		private void EndElement()
		{
			handler.EndElement();
		}

		protected void HandleNestedElements(string parentName, int parentMinOccurs, string childName, int childMinOccurs)
		{
			BeginElement(parentName, parentMinOccurs);
			HandleElement(childName, childMinOccurs);
			EndElement();
		}

		protected void HandleSiblingElements(string firstName, int firstMinOccurs, string secondName, int secondMinOccurs)
		{
			HandleElement(firstName, firstMinOccurs);
			HandleElement(secondName, secondMinOccurs);
		}

		protected void HandleElementAttribute(string elementName, string attributeName)
		{
			BeginElement(elementName, 1);
			HandleAttribute(attributeName);
			EndElement();
		}

		private void HandleAttribute(string name)
		{
			XmlSchemaAttribute attribute = new XmlSchemaAttribute();
			attribute.Name = name;
			handler.BeginAttribute(attribute);
			handler.EndAttribute();
		}

		protected XmlNamespaceManager namespaceManager;
		protected XmlNode data;
		protected XmlNode templates;
		protected XmlNode labels;
		protected XmlNode state;
		protected XmlNode binds;
		protected XmlNode ui;

		private XmlDocument xform;
		private XFormGenerator generator;
		private Handler handler;
	}
}
