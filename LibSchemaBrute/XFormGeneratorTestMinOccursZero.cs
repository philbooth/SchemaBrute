// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Xml;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class XFormGeneratorTestMinOccursZero : XFormGeneratorTest
	{
		[SetUp]
		public void SetUp()
		{
			Initialise();
		}

		[Test]
		public void TestNestedElementMinOccursZero()
		{
			HandleNestedElements("foo", 0, "bar", 0);

			Assert.AreEqual(1, ui.SelectNodes("xf:trigger", namespaceManager).Count, "Wrong number of delete triggers");
			Assert.AreEqual("instance('instLabels')/elem_foo/delete", ui.SelectSingleNode("xf:trigger/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
			Assert.IsNotNull(labels.SelectSingleNode("elem_foo/delete"), "Bad label datum");
			Assert.AreEqual("Delete foo", labels.SelectSingleNode("elem_foo/delete/@label").Value, "Bad label datum [value]");
			Assert.AreEqual(1, ui.SelectNodes("xf:group/xf:trigger", namespaceManager).Count, "Wrong number of delete triggers");
			Assert.AreEqual("instance('instLabels')/elem_foo/elem_bar/delete", ui.SelectSingleNode("xf:group/xf:trigger/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
			Assert.IsNotNull(labels.SelectSingleNode("elem_foo/elem_bar/delete"), "Bad label datum");
			Assert.AreEqual("Delete bar", labels.SelectSingleNode("elem_foo/elem_bar/delete/@label").Value, "Bad label datum [value]");
		}

		[Test]
		public void TestSiblingElementMinOccursZero()
		{
			HandleSiblingElements("foo", 0, "bar", 0);

			Assert.AreEqual(2, ui.SelectNodes("xf:trigger", namespaceManager).Count, "Wrong number of delete triggers");
			Assert.AreEqual("instance('instLabels')/elem_foo/delete", ui.SelectSingleNode("xf:trigger[1]/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
			Assert.IsNotNull(labels.SelectSingleNode("elem_foo/delete"), "Bad label datum");
			Assert.AreEqual("Delete foo", labels.SelectSingleNode("elem_foo/delete/@label").Value, "Bad label datum [value]");
			Assert.AreEqual("instance('instLabels')/elem_bar/delete", ui.SelectSingleNode("xf:trigger[2]/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
			Assert.IsNotNull(labels.SelectSingleNode("elem_bar/delete"), "Bad label datum");
			Assert.AreEqual("Delete bar", labels.SelectSingleNode("elem_bar/delete/@label").Value, "Bad label datum [value]");
		}
	}
}
