// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections;
using System.Xml.Schema;

namespace LibSchemaBrute
{
	sealed class Navigator
	{
		public Navigator()
		{
			SetOptions(new NavigatorOptions());
		}

		public Navigator(NavigatorOptions navigatorOptions)
		{
			SetOptions(navigatorOptions);
		}

		public void SetOptions(NavigatorOptions navigatorOptions)
		{
			options = navigatorOptions;
		}

		public void LoadSchema(string path)
		{
			schemaSet.Add(null, path);
		}

		public void Navigate(Handler schemaHandler)
		{
			handler = schemaHandler;

			schemaSet.Compile();

			NavigateTypes();

			NavigateElements();
		}

		private void NavigateTypes()
		{
			if(options.NavigateTypes)
				NavigateSchemaTypes();
		}

		private void NavigateSchemaTypes()
		{
			foreach(XmlSchemaType type in schemaSet.GlobalTypes.Values)
				NavigateType(type);
		}

		private void NavigateElements()
		{
			if(options.NavigateElements)
				NavigateSchemaElements();
		}

		private void NavigateSchemaElements()
		{
			foreach(XmlSchemaElement element in schemaSet.GlobalElements.Values)
				NavigateElement(element);
		}

		private void NavigateElement(XmlSchemaElement element)
		{
			handler.BeginElement(element);

			if(element.SchemaType != null)
				NavigateType(element.SchemaType);
			else if(element.ElementSchemaType != null)
				NavigateType(element.ElementSchemaType);

			handler.EndElement();
		}

		private void NavigateType(XmlSchemaType type)
		{
			if(type is XmlSchemaSimpleType)
				NavigateSimpleType((XmlSchemaSimpleType)type);
			else
				NavigateComplexType((XmlSchemaComplexType)type);
		}

		private void NavigateSimpleType(XmlSchemaSimpleType simpleType)
		{
			handler.BeginSimpleType(simpleType);

			NavigateSimpleTypeContent(simpleType.Content);

			handler.EndSimpleType();
		}

		private void NavigateSimpleTypeContent(XmlSchemaSimpleTypeContent simpleTypeContent)
		{
			// TODO: Refactor to SimpleTypeContentHandlerMap?
			if(simpleTypeContent is XmlSchemaSimpleTypeRestriction)
				NavigateSimpleTypeRestriction((XmlSchemaSimpleTypeRestriction)simpleTypeContent);
			else if(simpleTypeContent is XmlSchemaSimpleTypeUnion)
				NavigateSimpleTypeUnion((XmlSchemaSimpleTypeUnion)simpleTypeContent);
			else if(simpleTypeContent is XmlSchemaSimpleTypeList)
				NavigateSimpleTypeList((XmlSchemaSimpleTypeList)simpleTypeContent);
		}

		private void NavigateSimpleTypeRestriction(XmlSchemaSimpleTypeRestriction simpleTypeRestriction)
		{
			handler.BeginSimpleTypeRestriction(simpleTypeRestriction);

			NavigateObjectCollection(simpleTypeRestriction.Facets);

			handler.EndSimpleTypeRestriction();
		}

		private void NavigateSimpleTypeUnion(XmlSchemaSimpleTypeUnion simpleTypeUnion)
		{
			handler.BeginSimpleTypeUnion(simpleTypeUnion);

			foreach(XmlSchemaSimpleType simpleType in simpleTypeUnion.BaseMemberTypes)
				NavigateSimpleType(simpleType);

			handler.EndSimpleTypeUnion();
		}

		private void NavigateSimpleTypeList(XmlSchemaSimpleTypeList simpleTypeList)
		{
			handler.BeginSimpleTypeList(simpleTypeList);

			if(simpleTypeList.BaseItemType != null)
				NavigateSimpleType(simpleTypeList.BaseItemType);

			if(simpleTypeList.ItemType != null)
				NavigateSimpleType(simpleTypeList.ItemType);

			handler.EndSimpleTypeList();
		}

		private void NavigateComplexType(XmlSchemaComplexType complexType)
		{
			handler.BeginComplexType(complexType);

			NavigateObjectCollection(complexType.Attributes);

			if(complexType.AnyAttribute != null)
				NavigateAnyAttribute(complexType.AnyAttribute);

			if(complexType.Particle != null)
				NavigateParticle(complexType.Particle);
			else if(options.ResolveTypes && complexType.ContentModel != null)
			{
				NavigateContentModel(complexType.ContentModel);
				NavigateParticle(complexType.ContentTypeParticle);
			}

			handler.EndComplexType();
		}

		private void NavigateContentModel(XmlSchemaContentModel contentModel)
		{
			if(contentModel is XmlSchemaSimpleContent)
				NavigateSimpleContent((XmlSchemaSimpleContent)contentModel);
			else
				NavigateComplexContent((XmlSchemaComplexContent)contentModel);
		}

		private void NavigateSimpleContent(XmlSchemaSimpleContent simpleContent)
		{
			if(simpleContent.Content is XmlSchemaSimpleContentExtension)
				NavigateSimpleContentExtension((XmlSchemaSimpleContentExtension)simpleContent.Content);
			else
				NavigateSimpleContentRestriction((XmlSchemaSimpleContentRestriction)simpleContent.Content);
		}

		private void NavigateSimpleContentExtension(XmlSchemaSimpleContentExtension simpleContentExtension)
		{
			handler.BeginSimpleContentExtension(simpleContentExtension);

			NavigateObjectCollection(simpleContentExtension.Attributes);

			handler.EndSimpleContentExtension();
		}

		private void NavigateSimpleContentRestriction(XmlSchemaSimpleContentRestriction simpleContentRestriction)
		{
			handler.BeginSimpleContentRestriction(simpleContentRestriction);

			NavigateObjectCollection(simpleContentRestriction.Attributes);

			handler.EndSimpleContentRestriction();
		}

		private void NavigateComplexContent(XmlSchemaComplexContent complexContent)
		{
			if(complexContent.Content is XmlSchemaComplexContentExtension)
				NavigateComplexContentExtension((XmlSchemaComplexContentExtension)complexContent.Content);
			else
				NavigateComplexContentRestriction((XmlSchemaComplexContentRestriction)complexContent.Content);
		}

		private void NavigateComplexContentExtension(XmlSchemaComplexContentExtension complexContentExtension)
		{
			handler.BeginComplexContentExtension(complexContentExtension);

			NavigateParticle(complexContentExtension.Particle);

			handler.EndComplexContentExtension();
		}

		private void NavigateComplexContentRestriction(XmlSchemaComplexContentRestriction complexContentRestriction)
		{
			handler.BeginComplexContentRestriction(complexContentRestriction);

			NavigateParticle(complexContentRestriction.Particle);

			handler.EndComplexContentRestriction();
		}

		private void NavigateParticle(XmlSchemaParticle particle)
		{
			if(particle is XmlSchemaSequence)
				NavigateSequence((XmlSchemaSequence)particle);
			else if(particle is XmlSchemaChoice)
				NavigateChoice((XmlSchemaChoice)particle);
			else if(particle is XmlSchemaAll)
				NavigateAll((XmlSchemaAll)particle);
		}

		private void NavigateSequence(XmlSchemaSequence sequence)
		{
			handler.BeginSequence(sequence);

			NavigateObjectCollection(sequence.Items);

			handler.EndSequence();
		}

		private void NavigateChoice(XmlSchemaChoice choice)
		{
			handler.BeginChoice(choice);

			NavigateObjectCollection(choice.Items);

			handler.EndChoice();
		}

		private void NavigateAll(XmlSchemaAll all)
		{
			handler.BeginAll(all);

			NavigateObjectCollection(all.Items);

			handler.EndAll();
		}

		private void NavigateObjectCollection(XmlSchemaObjectCollection objects)
		{
			foreach(XmlSchemaObject obj in objects)
				NavigateObject(obj);
		}

		private void NavigateObject(XmlSchemaObject obj)
		{
			if(obj is XmlSchemaElement)
				NavigateElement((XmlSchemaElement)obj);
			else if(obj is XmlSchemaAttribute)
				NavigateAttribute((XmlSchemaAttribute)obj);
			else if(obj is XmlSchemaFacet)
				NavigateFacet((XmlSchemaFacet)obj);
			else if(obj is XmlSchemaAny)
				NavigateAny((XmlSchemaAny)obj);
			else if(obj is XmlSchemaAnyAttribute)
				NavigateAnyAttribute((XmlSchemaAnyAttribute)obj);
			else
				throw new ArgumentException("Invalid argument type in Navigator::NavigateObject");
		}

		private void NavigateAttribute(XmlSchemaAttribute attribute)
		{
			handler.BeginAttribute(attribute);

			if(attribute.AttributeSchemaType != null)
				NavigateSimpleType(attribute.AttributeSchemaType);

			handler.EndAttribute();
		}

		private void NavigateAny(XmlSchemaAny any)
		{
			handler.BeginAny(any);

			handler.EndAny();
		}

		private void NavigateAnyAttribute(XmlSchemaAnyAttribute anyAttribute)
		{
			handler.BeginAnyAttribute(anyAttribute);

			handler.EndAnyAttribute();
		}

		private void NavigateFacet(XmlSchemaFacet facet)
		{
			FacetHandlerMap.Handle(facet, handler);
		}

		private XmlSchemaSet schemaSet = new XmlSchemaSet();
		private Handler handler;
		private NavigatorOptions options;
	}
}
