// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class InstanceGeneratorTest
	{
		[SetUp]
		public void SetUp()
		{
			generator = new InstanceGenerator();
			handler = (Handler)generator;
			schemata = new XmlSchemaSet();
		}

		// TODO: Do some serious refactoring to these unit tests.

		[Test]
		public void TestInheritsHandler()
		{
			Assert.IsInstanceOf<Handler>(generator);
		}

		[Test]
		public void TestEmptyResult()
		{
			Assert.IsInstanceOf<XmlDocument>(generator.Result);
		}

		[Test]
		public void TestElementFoo()
		{
			CreateHandleAndAssertOneElement("foo");
		}

		[Test]
		public void TestElementBar()
		{
			CreateHandleAndAssertOneElement("bar");
		}

		private void CreateHandleAndAssertOneElement(string name)
		{
			CreateAndHandleElementWithoutValue(name);

			AssertOneElement(name, "");
		}

		private void CreateAndHandleElementWithoutValue(string name)
		{
			CreateAndHandleElement(name, null, null);
		}

		private void CreateAndHandleElement(string name, string defaultValue, string fixedValue)
		{
			XmlSchemaElement element = new XmlSchemaElement();

			element.Name = name;
			if(defaultValue != null)
				element.DefaultValue = defaultValue;
			if(fixedValue != null)
				element.FixedValue = fixedValue;

			handler.BeginElement(element);
		}

		private void AssertOneElement(string name, string ns)
		{
			Assert.AreEqual(1, GetResultsRoot().Count, "Instance generator returned incorrect number of elements");
			AssertElement((XmlElement)GetResultsRoot().Item(0), name, ns);
		}

		private XmlNodeList GetResultsRoot()
		{
			return generator.Result.SelectNodes("/*");
		}

		private void AssertElement(XmlElement element, string name, string ns)
		{
			AssertNode(element, name, ns, "element");
		}

		private void AssertNode(XmlNode node, string name, string ns, string type)
		{
			Assert.AreEqual(name, node.Name, "Instance generator returned an incorrectly-named " + type);
			Assert.AreEqual(ns, node.NamespaceURI, "Instance generator returned an incorrectly-namespaced " + type);
		}

		[Test]
		public void TestEmptyElement()
		{
			HandleNameElement("emptyElement.xsd", "");
		}

		[Test]
		public void TestNamespacedElement()
		{
			HandleNameElement("namespacedElement.xsd", "http://example.com/");
		}

		private void HandleNameElement(string schema, string ns)
		{
			LoadSchema(schema);

			foreach(XmlSchemaElement element in schemata.GlobalElements.Values)
			{
				handler.BeginElement(element);
				XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;
				if(complexType != null)
				{
					foreach(XmlSchemaObject obj in complexType.Attributes)
						HandleAttribute((XmlSchemaAttribute)obj);
				}
			}

			AssertOneElement("Name", ns);
		}

		private void LoadSchema(string filename)
		{
			schemata.Add(null, TestData.Path + filename);
			schemata.Compile();
		}

		private void HandleAttribute(XmlSchemaAttribute attribute)
		{
			handler.BeginAttribute(attribute);
			handler.EndAttribute();
		}

		[Test]
		public void TestNestedElements()
		{
			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleElementWithoutValue("bar");
			handler.EndElement();
			CreateAndHandleElementWithoutValue("baz");

			AssertOneElement("foo", "");
			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild, "bar", "");
			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild.NextSibling, "baz", "");
		}

		[Test]
		public void TestChoiceWithElements()
		{
			CreateAndHandleElementWithoutValue("foo");
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleElementWithoutValue("bar");
			handler.EndElement();
			CreateAndHandleElementWithoutValue("baz");

			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild, "bar", "");
			Assert.IsNull(GetResultsRoot().Item(0).FirstChild.NextSibling, "Instance generator returned an unexpected element");
		}

		[Test]
		public void TestChoiceWithSiblingElement()
		{
			CreateAndHandleElementWithoutValue("foo");
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleElementWithoutValue("bar");
			handler.EndElement();
			CreateAndHandleElementWithoutValue("baz");
			handler.EndElement();
			handler.EndChoice();
			CreateAndHandleElementWithoutValue("oof");

			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild, "bar", "");
			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild.NextSibling, "oof", "");
		}

		[Test]
		public void TestAttributes()
		{
			HandleNameElement("attributes.xsd", "");

			AssertAttribute((XmlAttribute)GetResultsRoot().Item(0).Attributes.Item(0), "FirstName", "");
			AssertAttribute((XmlAttribute)GetResultsRoot().Item(0).Attributes.Item(1), "Surname", "");
		}

		[Test]
		public void TestNamespacedAttributes()
		{
			HandleNameElement("namespacedAttributes.xsd", "http://example.com/");

			AssertAttribute((XmlAttribute)GetResultsRoot().Item(0).Attributes.Item(0), "FirstName", "http://example.com/");
			AssertAttribute((XmlAttribute)GetResultsRoot().Item(0).Attributes.Item(1), "Surname", "http://example.com/");
		}

		[Test]
		public void TestChoiceWithAttributes()
		{
			CreateAndHandleElementWithoutValue("foo");
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleAttributeWithoutValue("bar");
			handler.EndAttribute();
			CreateAndHandleAttributeWithoutValue("baz");

			AssertAttribute((XmlAttribute)GetResultsRoot().Item(0).Attributes.Item(0), "bar", "");
			Assert.IsNull(GetResultsRoot().Item(0).Attributes.Item(1), "Instance generator returned an unexpected attribute");
		}

		private void CreateAndHandleAttributeWithoutValue(string name)
		{
			CreateAndHandleAttribute(name, null, null);
		}

		private void CreateAndHandleAttribute(string name, string defaultValue, string fixedValue)
		{
			XmlSchemaAttribute attribute = new XmlSchemaAttribute();

			attribute.Name = name;
			if(defaultValue != null)
				attribute.DefaultValue = defaultValue;
			if(fixedValue != null)
				attribute.FixedValue = fixedValue;

			handler.BeginAttribute(attribute);
		}

		private void AssertAttribute(XmlAttribute attribute, string name, string ns)
		{
			AssertNode(attribute, name, ns, "attribute");
		}

		[Test]
		public void TestSiblingChoicesWithElements()
		{
			CreateAndHandleElementWithoutValue("foo");
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleElementWithoutValue("bar");
			handler.EndElement();
			CreateAndHandleElementWithoutValue("baz");
			handler.EndElement();
			handler.EndChoice();
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleElementWithoutValue("oof");
			handler.EndElement();
			CreateAndHandleElementWithoutValue("rab");

			AssertElement((XmlElement)GetResultsRoot().Item(0).ChildNodes.Item(0), "bar", "");
			AssertElement((XmlElement)GetResultsRoot().Item(0).ChildNodes.Item(1), "oof", "");
			Assert.IsNull(GetResultsRoot().Item(0).ChildNodes.Item(2), "Instance generator returned an unexpected element");
		}

		[Test]
		public void TestSiblingChoicesWithAttributes()
		{
			CreateAndHandleElementWithoutValue("foo");
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleAttributeWithoutValue("bar");
			handler.EndAttribute();
			CreateAndHandleAttributeWithoutValue("baz");
			handler.EndAttribute();
			handler.EndChoice();
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleAttributeWithoutValue("oof");
			handler.EndAttribute();
			CreateAndHandleAttributeWithoutValue("rab");

			AssertAttribute((XmlAttribute)GetResultsRoot().Item(0).Attributes.Item(0), "bar", "");
			AssertAttribute((XmlAttribute)GetResultsRoot().Item(0).Attributes.Item(1), "oof", "");
			Assert.IsNull(GetResultsRoot().Item(0).Attributes.Item(2), "Instance generator returned an unexpected attribute");
		}

		[Test]
		public void TestElementWithoutValue()
		{
			CreateAndHandleElementWithoutValue("foo");

			Assert.AreEqual("", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestAttributeWithoutValue()
		{
			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttributeWithoutValue("bar");
			handler.EndAttribute();

			Assert.AreEqual("", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect attribute value");
		}

		[Test]
		public void TestElementWithDefaultValue()
		{
			CreateAndHandleElement("foo", "bar", null);
			handler.EndElement();

			Assert.AreEqual("bar", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestAttributeWithDefaultValue()
		{
			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttribute("bar", "baz", null);
			handler.EndAttribute();

			Assert.AreEqual("baz", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect attribute value");
		}

		[Test]
		public void TestElementWithFixedValue()
		{
			CreateAndHandleElement("foo", null, "bar");
			handler.EndElement();

			Assert.AreEqual("bar", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestAttributeWithFixedValue()
		{
			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttribute("bar", null, "baz");
			handler.EndAttribute();

			Assert.AreEqual("baz", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect attribute value");
		}

		[Test]
		public void TestElementWithEnumeration()
		{
			CreateAndHandleElementWithoutValue("foo");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "bar";
			handler.Enumeration(enumeration);
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			handler.EndElement();

			Assert.AreEqual("bar", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestAttributeWithEnumeration()
		{
			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttributeWithoutValue("bar");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			enumeration.Value = "oof";
			handler.Enumeration(enumeration);
			handler.EndAttribute();

			Assert.AreEqual("baz", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestChoiceWithNestedElements()
		{
			CreateAndHandleElementWithoutValue("foo");
			handler.BeginChoice(new XmlSchemaChoice());
			CreateAndHandleElementWithoutValue("bar");
			CreateAndHandleElementWithoutValue("baz");
			handler.EndElement();
			CreateAndHandleElementWithoutValue("oof");
			handler.EndElement();
			handler.EndElement();
			CreateAndHandleElementWithoutValue("rab");
			CreateAndHandleElementWithoutValue("zab");
			handler.EndElement();
			CreateAndHandleElementWithoutValue("cor");

			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild, "bar", "");
			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild.ChildNodes.Item(0), "baz", "");
			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild.ChildNodes.Item(1), "oof", "");
			Assert.IsNull(GetResultsRoot().Item(0).FirstChild.ChildNodes.Item(1).NextSibling, "Instance generator returned an unexpected element");
			Assert.IsNull(GetResultsRoot().Item(0).FirstChild.NextSibling, "Instance generator returned an unexpected element");
		}

		[Test]
		public void TestEnumeratedElementWithMatchedDefaultValue()
		{
			CreateAndHandleElement("foo", "baz", null);
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "bar";
			handler.Enumeration(enumeration);
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			handler.EndElement();

			Assert.AreEqual("baz", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestEnumeratedElementWithUnmatchedDefaultValue()
		{
			// TODO: Decide whether this condition should actually fail fast / throw.

			CreateAndHandleElement("foo", "oof", null);
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "bar";
			handler.Enumeration(enumeration);
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			handler.EndElement();

			Assert.AreEqual("bar", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestEnumeratedAttributeWithMatchedDefaultValue()
		{
			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttribute("bar", "oof", null);
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			enumeration.Value = "oof";
			handler.Enumeration(enumeration);
			handler.EndAttribute();
			handler.EndElement();

			Assert.AreEqual("oof", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect attribute value");
		}

		[Test]
		public void TestEnumeratedAttributeWithUnmatchedDefaultValue()
		{
			// TODO: Decide whether this condition should actually fail fast / throw.

			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttribute("bar", "rab", null);
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			enumeration.Value = "oof";
			handler.Enumeration(enumeration);
			handler.EndAttribute();
			handler.EndElement();

			Assert.AreEqual("baz", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect attribute value");
		}

		[Test]
		public void TestEnumeratedElementWithMatchedFixedValue()
		{
			CreateAndHandleElement("foo", null, "baz");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "bar";
			handler.Enumeration(enumeration);
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			handler.EndElement();

			Assert.AreEqual("baz", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestEnumeratedElementWithUnmatchedFixedValue()
		{
			// TODO: Decide whether this condition should actually fail fast / throw.

			CreateAndHandleElement("foo", null, "oof");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "bar";
			handler.Enumeration(enumeration);
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			handler.EndElement();

			Assert.AreEqual("oof", GetResultsRoot().Item(0).InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestEnumeratedAttributeWithMatchedFixedValue()
		{
			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttribute("bar", null, "oof");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			enumeration.Value = "oof";
			handler.Enumeration(enumeration);
			handler.EndAttribute();
			handler.EndElement();

			Assert.AreEqual("oof", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect attribute value");
		}

		[Test]
		public void TestEnumeratedAttributeWithUnmatchedFixedValue()
		{
			// TODO: Decide whether this condition should actually fail fast / throw.

			CreateAndHandleElementWithoutValue("foo");
			CreateAndHandleAttribute("bar", null, "rab");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			enumeration.Value = "oof";
			handler.Enumeration(enumeration);
			handler.EndAttribute();
			handler.EndElement();

			Assert.AreEqual("rab", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect attribute value");
		}

		[Test]
		public void TestEnumeratedElementSiblings()
		{
			CreateAndHandleElementWithoutValue("root");

			CreateAndHandleElementWithoutValue("foo");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "bar";
			handler.Enumeration(enumeration);
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			handler.EndElement();

			CreateAndHandleElementWithoutValue("oof");
			enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "rab";
			handler.Enumeration(enumeration);
			enumeration.Value = "zab";
			handler.Enumeration(enumeration);
			handler.EndElement();

			Assert.AreEqual("bar", GetResultsRoot().Item(0).FirstChild.InnerXml, "Instance generator returned an incorrect element value");
			Assert.AreEqual("rab", GetResultsRoot().Item(0).FirstChild.NextSibling.InnerXml, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestEnumeratedAttributeSiblings()
		{
			CreateAndHandleElementWithoutValue("foo");

			CreateAndHandleAttributeWithoutValue("bar");
			XmlSchemaEnumerationFacet enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "baz";
			handler.Enumeration(enumeration);
			enumeration.Value = "oof";
			handler.Enumeration(enumeration);
			handler.EndAttribute();

			CreateAndHandleAttributeWithoutValue("rab");
			enumeration = new XmlSchemaEnumerationFacet();
			enumeration.Value = "zab";
			handler.Enumeration(enumeration);
			enumeration.Value = "cor";
			handler.Enumeration(enumeration);
			handler.EndAttribute();

			Assert.AreEqual("baz", GetResultsRoot().Item(0).Attributes.Item(0).Value, "Instance generator returned an incorrect element value");
			Assert.AreEqual("zab", GetResultsRoot().Item(0).Attributes.Item(1).Value, "Instance generator returned an incorrect element value");
		}

		[Test]
		public void TestNestedChoices()
		{
			CreateAndHandleElementWithoutValue("foo");
			handler.BeginChoice(new XmlSchemaChoice());

			CreateAndHandleElementWithoutValue("bar");
			CreateAndHandleChoiceOfTwoElements("baz", "oof");
			handler.EndElement();

			CreateAndHandleElementWithoutValue("rab");
			CreateAndHandleChoiceOfTwoElements("zab", "cor");
			handler.EndElement();

			handler.EndChoice();
			handler.EndElement();

			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild, "bar", "");
			AssertElement((XmlElement)GetResultsRoot().Item(0).FirstChild.FirstChild, "baz", "");
			Assert.IsNull(GetResultsRoot().Item(0).FirstChild.FirstChild.NextSibling, "Instance generator returned an unexpected element");
			Assert.IsNull(GetResultsRoot().Item(0).FirstChild.NextSibling, "Instance generator returned an unexpected element");
		}

		private void CreateAndHandleChoiceOfTwoElements(string name1, string name2)
		{
			handler.BeginChoice(new XmlSchemaChoice());

			CreateAndHandleElementWithoutValue(name1);
			handler.EndElement();

			CreateAndHandleElementWithoutValue(name2);
			handler.EndElement();

			handler.EndChoice();
		}

		[Test]
		public void TestContextElement()
		{
			XmlDocument instance = new XmlDocument();
			instance.LoadXml("<foo><bar /></foo>");
			generator = new InstanceGenerator(instance, instance.SelectSingleNode("/foo/bar"));
			handler = (Handler)generator;

			XmlSchemaElement element = new XmlSchemaElement();
			element.Name = "baz";
			handler.BeginElement(element);

			Assert.AreSame(instance, generator.Result, "Instance generator returned incorrect instance document");
			Assert.AreEqual(1, generator.Result.SelectNodes("/foo/bar/*").Count, "Instance generator returned incorrect number of elements");
			AssertElement((XmlElement)generator.Result.SelectNodes("/foo/bar/*").Item(0), "baz", "");
		}

		[Test]
		public void TestContextAttributeThrows()
		{
			XmlDocument instance = new XmlDocument();
			instance.LoadXml("<foo><bar baz=\"blah\" /></foo>");
			Assert.Throws<ArgumentException>(delegate { generator = new InstanceGenerator(instance, instance.SelectSingleNode("/foo/bar/@baz")); });
		}

		private InstanceGenerator generator;
		private Handler handler;
		private XmlSchemaSet schemata;
	}
}
