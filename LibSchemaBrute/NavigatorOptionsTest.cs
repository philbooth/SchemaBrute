// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class NavigatorOptionsTest
	{
		[Test]
		public void NavigateTypesTrue()
		{
			NavigatorOptions options = new NavigatorOptions(true, false, false);
			Assert.AreEqual(true, options.NavigateTypes);
		}

		[Test]
		public void NavigateTypesFalse()
		{
			NavigatorOptions options = new NavigatorOptions(false, false, false);
			Assert.AreEqual(false, options.NavigateTypes);
		}

		[Test]
		public void NavigateElementsTrue()
		{
			NavigatorOptions options = new NavigatorOptions(false, true, false);
			Assert.AreEqual(true, options.NavigateElements);
		}

		[Test]
		public void NavigateElementsFalse()
		{
			NavigatorOptions options = new NavigatorOptions(false, false, false);
			Assert.AreEqual(false, options.NavigateElements);
		}

		[Test]
		public void ResolveTypesTrue()
		{
			NavigatorOptions options = new NavigatorOptions(false, false, true);
			Assert.AreEqual(true, options.ResolveTypes);
		}

		[Test]
		public void ResolveTypesFalse()
		{
			NavigatorOptions options = new NavigatorOptions(false, false, false);
			Assert.AreEqual(false, options.ResolveTypes);
		}

		[Test]
		public void DefaultConstruction()
		{
			NavigatorOptions options = new NavigatorOptions();
			Assert.AreEqual(false, options.NavigateTypes);
			Assert.AreEqual(true, options.NavigateElements);
			Assert.AreEqual(true, options.ResolveTypes);
		}
	}
}
