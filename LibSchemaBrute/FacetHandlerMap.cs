// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Xml.Schema;

namespace LibSchemaBrute
{
	sealed class FacetHandlerMap
	{
		public static void Handle(XmlSchemaFacet facet, Handler handlerArg)
		{
			SetHandler(handlerArg);

			map[facet.GetType()](facet);
		}

		private FacetHandlerMap()
		{
		}

		private static void SetHandler(Handler newHandler)
		{
			if(handler != newHandler)
			{
				handler = newHandler;
				Initialise();
			}
		}

		private static void Initialise()
		{
			map.Clear();

			map.Add(typeof(XmlSchemaLengthFacet), handler.Length);
			map.Add(typeof(XmlSchemaMinLengthFacet), handler.MinLength);
			map.Add(typeof(XmlSchemaMaxLengthFacet), handler.MaxLength);
			map.Add(typeof(XmlSchemaPatternFacet), handler.Pattern);
			map.Add(typeof(XmlSchemaEnumerationFacet), handler.Enumeration);
			map.Add(typeof(XmlSchemaMinInclusiveFacet), handler.MinInclusive);
			map.Add(typeof(XmlSchemaMaxInclusiveFacet), handler.MaxInclusive);
			map.Add(typeof(XmlSchemaMinExclusiveFacet), handler.MinExclusive);
			map.Add(typeof(XmlSchemaMaxExclusiveFacet), handler.MaxExclusive);
			map.Add(typeof(XmlSchemaTotalDigitsFacet), handler.TotalDigits);
			map.Add(typeof(XmlSchemaFractionDigitsFacet), handler.FractionDigits);
			map.Add(typeof(XmlSchemaWhiteSpaceFacet), handler.WhiteSpace);
		}

		private static Handler handler;
		private static Dictionary<Type, Action<XmlSchemaFacet>> map = new Dictionary<Type, Action<XmlSchemaFacet>>();
	}
}
