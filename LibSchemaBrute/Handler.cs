// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System.Xml.Schema;

namespace LibSchemaBrute
{
	interface Handler
	{
		void BeginElement(XmlSchemaElement element);
		void EndElement();
		void BeginSequence(XmlSchemaSequence sequence);
		void EndSequence();
		void BeginChoice(XmlSchemaChoice choice);
		void EndChoice();
		void BeginAll(XmlSchemaAll all);
		void EndAll();
		void BeginGroup(XmlSchemaGroup group);
		void EndGroup();
		void BeginSimpleType(XmlSchemaSimpleType simpleType);
		void EndSimpleType();
		void BeginSimpleTypeRestriction(XmlSchemaSimpleTypeRestriction simpleTypeRestriction);
		void EndSimpleTypeRestriction();
		void Length(XmlSchemaFacet length);
		void MinLength(XmlSchemaFacet minLength);
		void MaxLength(XmlSchemaFacet maxLength);
		void Pattern(XmlSchemaFacet pattern);
		void Enumeration(XmlSchemaFacet enumeration);
		void MinInclusive(XmlSchemaFacet minInclusive);
		void MaxInclusive(XmlSchemaFacet maxInclusive);
		void MinExclusive(XmlSchemaFacet minExclusive);
		void MaxExclusive(XmlSchemaFacet maxExclusive);
		void TotalDigits(XmlSchemaFacet totalDigits);
		void FractionDigits(XmlSchemaFacet fractionDigits);
		void WhiteSpace(XmlSchemaFacet whiteSpace);
		void BeginSimpleTypeUnion(XmlSchemaSimpleTypeUnion union);
		void EndSimpleTypeUnion();
		void BeginSimpleTypeList(XmlSchemaSimpleTypeList list);
		void EndSimpleTypeList();
		void BeginComplexType(XmlSchemaComplexType complexType);
		void EndComplexType();
		void BeginAttribute(XmlSchemaAttribute attribute);
		void EndAttribute();
		void BeginAny(XmlSchemaAny any);
		void EndAny();
		void BeginAnyAttribute(XmlSchemaAnyAttribute anyAttribute);
		void EndAnyAttribute();
		void BeginSimpleContentExtension(XmlSchemaSimpleContentExtension simpleContentExtension);
		void EndSimpleContentExtension();
		void BeginSimpleContentRestriction(XmlSchemaSimpleContentRestriction simpleContentRestriction);
		void EndSimpleContentRestriction();
		void BeginComplexContentExtension(XmlSchemaComplexContentExtension complexContentExtension);
		void EndComplexContentExtension();
		void BeginComplexContentRestriction(XmlSchemaComplexContentRestriction complexContentRestriction);
		void EndComplexContentRestriction();
	}
}
