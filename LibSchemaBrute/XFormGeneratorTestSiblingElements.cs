// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Xml;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class XFormGeneratorTestSiblingElements : XFormGeneratorTest
	{
		[SetUp]
		public void SetUp()
		{
			Initialise();
			HandleSiblingElements("foo", 1, "bar", 1);
		}

		[Test]
		public void TestSiblingElementData()
		{
			Assert.AreEqual(2, data.SelectNodes("*").Count, "Wrong number of data nodes");
			Assert.AreEqual("foo", data.SelectSingleNode("*[1]").Name, "Bad datum [name]");
			Assert.AreEqual("bar", data.SelectSingleNode("*[2]").Name, "Bad datum [name]");
		}

		[Test]
		public void TestSiblingElementLabels()
		{
			Assert.AreEqual(2, labels.SelectNodes("*").Count, "Wrong number of label nodes");
			Assert.AreEqual("elem_foo", labels.SelectSingleNode("*[1]").Name, "Bad label datum [name]");
			Assert.AreEqual("foo", labels.SelectSingleNode("elem_foo/@label").Value, "Bad label datum [value]");
			Assert.AreEqual("elem_bar", labels.SelectSingleNode("*[2]").Name, "Bad label datum [name]");
			Assert.AreEqual("bar", labels.SelectSingleNode("elem_bar/@label").Value, "Bad label datum [value]");
		}

		[Test]
		public void TestSiblingElementBinds()
		{
			Assert.AreEqual(2, binds.SelectNodes("*").Count, "Wrong number of binds");
			Assert.AreEqual("xf:bind", binds.SelectSingleNode("*[1]").Name, "Bad bind [name]");
			Assert.AreEqual("xf:bind", binds.SelectSingleNode("*[2]", namespaceManager).Name, "Bad bind [name]");
			Assert.AreEqual("foo", binds.SelectSingleNode("xf:bind[1]/@nodeset", namespaceManager).Value, "Bad bind [nodeset]");
			Assert.AreEqual("bar", binds.SelectSingleNode("xf:bind[2]/@nodeset", namespaceManager).Value, "Bad bind [nodeset]");
		}

		[Test]
		public void TestSiblingElementUi()
		{
			Assert.AreEqual(0, ui.SelectNodes("xf:group", namespaceManager).Count, "Wrong number of groups");
			Assert.AreEqual(2, ui.SelectNodes("xf:input", namespaceManager).Count, "Wrong number of inputs");
			Assert.AreEqual(0, ui.SelectNodes("xf:trigger", namespaceManager).Count, "Wrong number of triggers");
			Assert.AreEqual("foo", ui.SelectSingleNode("xf:input[1]/@ref", namespaceManager).Value, "Bad input [ref]");
			Assert.AreEqual(1, ui.SelectNodes("xf:input[1]/xf:label", namespaceManager).Count, "Wrong number of labels");
			Assert.AreEqual("instance('instLabels')/elem_foo", ui.SelectSingleNode("xf:input[1]/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
			Assert.AreEqual("bar", ui.SelectSingleNode("xf:input[2]/@ref", namespaceManager).Value, "Bad input [ref]");
			Assert.AreEqual(1, ui.SelectNodes("xf:input[2]/xf:label", namespaceManager).Count, "Wrong number of labels");
			Assert.AreEqual("instance('instLabels')/elem_bar", ui.SelectSingleNode("xf:input[2]/xf:label/@ref", namespaceManager).Value, "Bad label XPath");
		}

		[Test]
		public void TestSiblingElementTemplates()
		{
			Assert.AreEqual(2, templates.SelectNodes("*").Count, "Wrong number of templates");
			Assert.AreEqual("foo", templates.SelectSingleNode("*[1]").Name, "Bad template [name]");
			Assert.AreEqual("bar", templates.SelectSingleNode("*[2]").Name, "Bad template [name]");
		}
	}
}
