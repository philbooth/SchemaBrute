// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;

namespace LibSchemaBrute
{
	class NavigatorOptions
	{
		bool navigateTypes = false;
		bool navigateElements = true;
		bool resolveTypes = true;

		public NavigatorOptions()
		{
		}

		public NavigatorOptions(bool navigateTypesArg, bool navigateElementsArg, bool resolveTypesArg)
		{
			navigateTypes = navigateTypesArg;
			navigateElements = navigateElementsArg;
			resolveTypes = resolveTypesArg;
		}

		public bool NavigateTypes
		{
			get
			{
				return navigateTypes;
			}
		}

		public bool NavigateElements
		{
			get
			{
				return navigateElements;
			}
		}

		public bool ResolveTypes
		{
			get
			{
				return resolveTypes;
			}
		}
	}
}
