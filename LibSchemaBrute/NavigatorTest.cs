// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using NUnit.Framework;

namespace LibSchemaBrute
{
	[TestFixture]
	class NavigatorTest
	{
		[SetUp]
		public void SetUp()
		{
			navigator = new Navigator();
			handler = new HandlerStub();
			expectedCounts.Reset();
		}

		[TearDown]
		public void TearDown()
		{
		}

		[Test]
		public void LoadEmptyPath()
		{
			Assert.Throws<ArgumentNullException>(delegate { navigator.LoadSchema(""); });
		}

		[Test]
		public void LoadMalformedSchema()
		{
			Assert.Throws<XmlException>(delegate { navigator.LoadSchema(TestData.Path + "malformed.xsd"); });
		}

		[Test]
		public void LoadInvalidSchema()
		{
			Assert.Throws<XmlSchemaException>(delegate { navigator.LoadSchema(TestData.Path + "invalid.xsd"); });
		}

		[Test]
		public void LoadValidSchema()
		{
			Assert.DoesNotThrow(delegate { navigator.LoadSchema(TestData.Path + "emptyElement.xsd"); });
		}

		[Test]
		public void NavigateEmptyElement()
		{
			navigator.LoadSchema(TestData.Path + "emptyElement.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSequence()
		{
			navigator.LoadSchema(TestData.Path + "sequence.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 3);
			expectedCounts.Set(Counts.Index.Sequence, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 3);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateChoice()
		{
			navigator.LoadSchema(TestData.Path + "choice.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 3);
			expectedCounts.Set(Counts.Index.Choice, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 3);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateAll()
		{
			navigator.LoadSchema(TestData.Path + "all.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 3);
			expectedCounts.Set(Counts.Index.All, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 3);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeString()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeString.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionLength()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionLength.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.Length, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.Length, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionMinLength()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionMinLength.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.MinLength, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MinLength, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionMaxLength()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionMaxLength.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.MaxLength, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MaxLength, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionPattern()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionPattern.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.Pattern, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.Pattern, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionEnumeration()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionEnumeration.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.Enumeration, 5);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.Enumeration, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionMinInclusive()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionMinInclusive.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.MinInclusive, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MinInclusive, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionMaxInclusive()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionMaxInclusive.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.MaxInclusive, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MaxInclusive, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionMinExclusive()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionMinExclusive.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.MinExclusive, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MinExclusive, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionMaxExclusive()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionMaxExclusive.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.MaxExclusive, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MaxExclusive, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionTotalDigits()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionTotalDigits.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.TotalDigits, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.TotalDigits, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionFractionDigits()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionFractionDigits.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.FractionDigits, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.FractionDigits, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeRestrictionWhiteSpace()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeRestrictionWhiteSpace.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.WhiteSpace, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.WhiteSpace, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeUnion()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeUnion.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 3);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 2);
			expectedCounts.Set(Counts.Index.MinInclusive, 1);
			expectedCounts.Set(Counts.Index.Pattern, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeUnion, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MinInclusive, 0);
			expectedCounts.Set(Counts.Index.Pattern, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeUnionSimpleType()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeUnionSimpleType.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 3);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 2);
			expectedCounts.Set(Counts.Index.MinExclusive, 1);
			expectedCounts.Set(Counts.Index.MaxExclusive, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeUnion, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.MinExclusive, 0);
			expectedCounts.Set(Counts.Index.MaxExclusive, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeList()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeList.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 2);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeList, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateSimpleTypeListSimpleType()
		{
			navigator.LoadSchema(TestData.Path + "simpleTypeListSimpleType.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 3);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 2);
			expectedCounts.Set(Counts.Index.Pattern, 2);
			expectedCounts.Set(Counts.Index.SimpleTypeList, 1);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.Pattern, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeSequence()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeSequence.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 3);
			expectedCounts.Set(Counts.Index.Sequence, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 3);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeSequenceAny()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeSequenceAny.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 2);
			expectedCounts.Set(Counts.Index.Sequence, 1);
			expectedCounts.Set(Counts.Index.Any, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 2);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeChoice()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeChoice.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 3);
			expectedCounts.Set(Counts.Index.Choice, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 3);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeAll()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeAll.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 3);
			expectedCounts.Set(Counts.Index.All, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 3);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateAttributes()
		{
			navigator.LoadSchema(TestData.Path + "attributes.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 2);
			expectedCounts.Set(Counts.Index.ComplexType, 1);
			expectedCounts.Set(Counts.Index.Attribute, 2);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateAttributesSimpleTypes()
		{
			navigator.LoadSchema(TestData.Path + "attributesSimpleTypes.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 3);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 3);
			expectedCounts.Set(Counts.Index.Enumeration, 5);
			expectedCounts.Set(Counts.Index.Pattern, 2);
			expectedCounts.Set(Counts.Index.ComplexType, 1);
			expectedCounts.Set(Counts.Index.Attribute, 3);
			handler.AssertCounts(expectedCounts);
			expectedCounts.Set(Counts.Index.Pattern, 0);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeSequenceAnyAttribute()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeSequenceAnyAttribute.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 3);
			expectedCounts.Set(Counts.Index.Sequence, 1);
			expectedCounts.Set(Counts.Index.AnyAttribute, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 3);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeSimpleContentExtension()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeSimpleContentExtension.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.SimpleType, 1);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 1);
			expectedCounts.Set(Counts.Index.Attribute, 1);
			expectedCounts.Set(Counts.Index.SimpleContentExtension, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeExtension()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeExtension.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 5);
			expectedCounts.Set(Counts.Index.Sequence, 2);
			expectedCounts.Set(Counts.Index.ComplexType, 5);
			expectedCounts.Set(Counts.Index.ComplexContentExtension, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeRestriction()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeRestriction.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 5);
			expectedCounts.Set(Counts.Index.Sequence, 2);
			expectedCounts.Set(Counts.Index.SimpleType, 4);
			expectedCounts.Set(Counts.Index.SimpleTypeRestriction, 4);
			expectedCounts.Set(Counts.Index.ComplexType, 1);
			expectedCounts.Set(Counts.Index.ComplexContentRestriction, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateComplexTypeComplexType()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeComplexType.xsd");
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 5);
			expectedCounts.Set(Counts.Index.Sequence, 2);
			expectedCounts.Set(Counts.Index.ComplexType, 5);
			expectedCounts.Set(Counts.Index.ComplexContentExtension, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateNothing()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeComplexType.xsd");
			navigator.SetOptions(new NavigatorOptions(false, false, true));
			navigator.Navigate(handler);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateTypesOnly()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeComplexType.xsd");
			navigator.SetOptions(new NavigatorOptions(true, false, true));
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 6);
			expectedCounts.Set(Counts.Index.Sequence, 3);
			expectedCounts.Set(Counts.Index.ComplexType, 9);
			expectedCounts.Set(Counts.Index.ComplexContentExtension, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		[Test]
		public void NavigateElementsWithoutResolvingTypes()
		{
			navigator.LoadSchema(TestData.Path + "complexTypeComplexType.xsd");
			navigator.SetOptions(new NavigatorOptions(false, true, false));
			navigator.Navigate(handler);
			expectedCounts.Set(Counts.Index.Element, 1);
			expectedCounts.Set(Counts.Index.ComplexType, 1);
			handler.AssertCounts(expectedCounts);
			handler.AssertEndCounts(expectedCounts);
		}

		private Navigator navigator;
		private HandlerStub handler;
		private Counts expectedCounts = new Counts();
	}
}
