// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System.Xml.Schema;
using NUnit.Framework;

namespace LibSchemaBrute
{
	class HandlerStub : Handler
	{
		private Counts counts = new Counts();
		private Counts endCounts = new Counts();

		void Handler.BeginElement(XmlSchemaElement element)
		{
			counts.Increment(Counts.Index.Element);
		}

		void Handler.EndElement()
		{
			endCounts.Increment(Counts.Index.Element);
		}

		void Handler.BeginSequence(XmlSchemaSequence sequence)
		{
			counts.Increment(Counts.Index.Sequence);
		}

		void Handler.EndSequence()
		{
			endCounts.Increment(Counts.Index.Sequence);
		}

		void Handler.BeginChoice(XmlSchemaChoice choice)
		{
			counts.Increment(Counts.Index.Choice);
		}

		void Handler.EndChoice()
		{
			endCounts.Increment(Counts.Index.Choice);
		}

		void Handler.BeginAll(XmlSchemaAll all)
		{
			counts.Increment(Counts.Index.All);
		}

		void Handler.EndAll()
		{
			endCounts.Increment(Counts.Index.All);
		}

		void Handler.BeginGroup(XmlSchemaGroup group)
		{
			counts.Increment(Counts.Index.Group);
		}

		void Handler.EndGroup()
		{
			endCounts.Increment(Counts.Index.Group);
		}

		void Handler.BeginSimpleType(XmlSchemaSimpleType simpleType)
		{
			counts.Increment(Counts.Index.SimpleType);
		}

		void Handler.EndSimpleType()
		{
			endCounts.Increment(Counts.Index.SimpleType);
		}

		void Handler.BeginSimpleTypeRestriction(XmlSchemaSimpleTypeRestriction simpleTypeRestriction)
		{
			counts.Increment(Counts.Index.SimpleTypeRestriction);
		}

		void Handler.EndSimpleTypeRestriction()
		{
			endCounts.Increment(Counts.Index.SimpleTypeRestriction);
		}

		void Handler.Length(XmlSchemaFacet length)
		{
			counts.Increment(Counts.Index.Length);
		}

		void Handler.MinLength(XmlSchemaFacet minLength)
		{
			counts.Increment(Counts.Index.MinLength);
		}

		void Handler.MaxLength(XmlSchemaFacet maxLength)
		{
			counts.Increment(Counts.Index.MaxLength);
		}

		void Handler.Pattern(XmlSchemaFacet pattern)
		{
			counts.Increment(Counts.Index.Pattern);
		}

		void Handler.Enumeration(XmlSchemaFacet enumeration)
		{
			counts.Increment(Counts.Index.Enumeration);
		}

		void Handler.MinInclusive(XmlSchemaFacet minInclusive)
		{
			counts.Increment(Counts.Index.MinInclusive);
		}

		void Handler.MaxInclusive(XmlSchemaFacet maxInclusive)
		{
			counts.Increment(Counts.Index.MaxInclusive);
		}

		void Handler.MinExclusive(XmlSchemaFacet minExclusive)
		{
			counts.Increment(Counts.Index.MinExclusive);
		}

		void Handler.MaxExclusive(XmlSchemaFacet maxExclusive)
		{
			counts.Increment(Counts.Index.MaxExclusive);
		}

		void Handler.TotalDigits(XmlSchemaFacet totalDigits)
		{
			counts.Increment(Counts.Index.TotalDigits);
		}

		void Handler.FractionDigits(XmlSchemaFacet fractionDigits)
		{
			counts.Increment(Counts.Index.FractionDigits);
		}

		void Handler.WhiteSpace(XmlSchemaFacet whiteSpace)
		{
			counts.Increment(Counts.Index.WhiteSpace);
		}

		void Handler.BeginSimpleTypeUnion(XmlSchemaSimpleTypeUnion union)
		{
			counts.Increment(Counts.Index.SimpleTypeUnion);
		}

		void Handler.EndSimpleTypeUnion()
		{
			endCounts.Increment(Counts.Index.SimpleTypeUnion);
		}

		void Handler.BeginSimpleTypeList(XmlSchemaSimpleTypeList list)
		{
			counts.Increment(Counts.Index.SimpleTypeList);
		}

		void Handler.EndSimpleTypeList()
		{
			endCounts.Increment(Counts.Index.SimpleTypeList);
		}

		void Handler.BeginComplexType(XmlSchemaComplexType complexType)
		{
			counts.Increment(Counts.Index.ComplexType);
		}

		void Handler.EndComplexType()
		{
			endCounts.Increment(Counts.Index.ComplexType);
		}

		void Handler.BeginAttribute(XmlSchemaAttribute attribute)
		{
			counts.Increment(Counts.Index.Attribute);
		}

		void Handler.EndAttribute()
		{
			endCounts.Increment(Counts.Index.Attribute);
		}

		void Handler.BeginAny(XmlSchemaAny any)
		{
			counts.Increment(Counts.Index.Any);
		}

		void Handler.EndAny()
		{
			endCounts.Increment(Counts.Index.Any);
		}

		void Handler.BeginAnyAttribute(XmlSchemaAnyAttribute anyAttribute)
		{
			counts.Increment(Counts.Index.AnyAttribute);
		}

		void Handler.EndAnyAttribute()
		{
			endCounts.Increment(Counts.Index.AnyAttribute);
		}

		void Handler.BeginSimpleContentExtension(XmlSchemaSimpleContentExtension simpleContentExtension)
		{
			counts.Increment(Counts.Index.SimpleContentExtension);
		}

		void Handler.EndSimpleContentExtension()
		{
			endCounts.Increment(Counts.Index.SimpleContentExtension);
		}

		void Handler.BeginSimpleContentRestriction(XmlSchemaSimpleContentRestriction simpleContentRestriction)
		{
			counts.Increment(Counts.Index.SimpleContentRestriction);
		}

		void Handler.EndSimpleContentRestriction()
		{
			endCounts.Increment(Counts.Index.SimpleContentRestriction);
		}

		void Handler.BeginComplexContentExtension(XmlSchemaComplexContentExtension complexContentExtension)
		{
			counts.Increment(Counts.Index.ComplexContentExtension);
		}

		void Handler.EndComplexContentExtension()
		{
			endCounts.Increment(Counts.Index.ComplexContentExtension);
		}

		void Handler.BeginComplexContentRestriction(XmlSchemaComplexContentRestriction complexContentRestriction)
		{
			counts.Increment(Counts.Index.ComplexContentRestriction);
		}

		void Handler.EndComplexContentRestriction()
		{
			endCounts.Increment(Counts.Index.ComplexContentRestriction);
		}

		public void AssertCounts(Counts expectedCounts)
		{
			AssertCount(Counts.Index.Element, expectedCounts, "elements");
			AssertCount(Counts.Index.Sequence, expectedCounts, "sequences");
			AssertCount(Counts.Index.Choice, expectedCounts, "choices");
			AssertCount(Counts.Index.All, expectedCounts, "alls");
			AssertCount(Counts.Index.Group, expectedCounts, "groups");
			AssertCount(Counts.Index.SimpleType, expectedCounts, "simple types");
			AssertCount(Counts.Index.SimpleTypeRestriction, expectedCounts, "simple type restrictions");
			AssertCount(Counts.Index.Length, expectedCounts, "lengths");
			AssertCount(Counts.Index.MinLength, expectedCounts, "minimum lengths");
			AssertCount(Counts.Index.MaxLength, expectedCounts, "maximum lengths");
			AssertCount(Counts.Index.Pattern, expectedCounts, "patterns");
			AssertCount(Counts.Index.Enumeration, expectedCounts, "enumerations");
			AssertCount(Counts.Index.MinInclusive, expectedCounts, "minimum inclusives");
			AssertCount(Counts.Index.MaxInclusive, expectedCounts, "maximum inclusives");
			AssertCount(Counts.Index.MinExclusive, expectedCounts, "minimum exclusives");
			AssertCount(Counts.Index.MaxExclusive, expectedCounts, "maximum exclusives");
			AssertCount(Counts.Index.TotalDigits, expectedCounts, "total digits'");
			AssertCount(Counts.Index.FractionDigits, expectedCounts, "fraction digits'");
			AssertCount(Counts.Index.WhiteSpace, expectedCounts, "white spaces");
			AssertCount(Counts.Index.SimpleTypeUnion, expectedCounts, "simple type unions");
			AssertCount(Counts.Index.SimpleTypeList, expectedCounts, "simple type lists");
			AssertCount(Counts.Index.ComplexType, expectedCounts, "complex types");
			AssertCount(Counts.Index.Attribute, expectedCounts, "attributes");
			AssertCount(Counts.Index.Any, expectedCounts, "anys");
			AssertCount(Counts.Index.AnyAttribute, expectedCounts, "anyAttributes");
			AssertCount(Counts.Index.SimpleContentExtension, expectedCounts, "simple content extensions");
			AssertCount(Counts.Index.ComplexContentExtension, expectedCounts, "complex content extensions");
			AssertCount(Counts.Index.ComplexContentRestriction, expectedCounts, "complex content restrictions");
		}

		private void AssertCount(Counts.Index index, Counts expectedCounts, string countName)
		{
			AssertCountsAreEqual(index, expectedCounts, counts, countName);
		}

		public void AssertEndCounts(Counts expectedCounts)
		{
			AssertEndCount(Counts.Index.Element, expectedCounts, "elements");
			AssertEndCount(Counts.Index.Sequence, expectedCounts, "sequences");
			AssertEndCount(Counts.Index.Choice, expectedCounts, "choices");
			AssertEndCount(Counts.Index.All, expectedCounts, "alls");
			AssertEndCount(Counts.Index.Group, expectedCounts, "groups");
			AssertEndCount(Counts.Index.SimpleType, expectedCounts, "simple types");
			AssertEndCount(Counts.Index.SimpleTypeRestriction, expectedCounts, "simple type restrictions");
			AssertEndCount(Counts.Index.SimpleTypeUnion, expectedCounts, "simple type unions");
			AssertEndCount(Counts.Index.SimpleTypeList, expectedCounts, "simple type lists");
			AssertEndCount(Counts.Index.ComplexType, expectedCounts, "complex types");
			AssertEndCount(Counts.Index.Attribute, expectedCounts, "attributes");
			AssertEndCount(Counts.Index.Any, expectedCounts, "anys");
			AssertEndCount(Counts.Index.AnyAttribute, expectedCounts, "anyAttributes");
			AssertEndCount(Counts.Index.SimpleContentExtension, expectedCounts, "simple content extensions");
			AssertEndCount(Counts.Index.ComplexContentExtension, expectedCounts, "complex content extensions");
			AssertEndCount(Counts.Index.ComplexContentRestriction, expectedCounts, "complex content restrictions");
		}

		private void AssertEndCount(Counts.Index index, Counts expectedCounts, string countName)
		{
			AssertCountsAreEqual(index, expectedCounts, endCounts, "end " + countName);
		}

		private static void AssertCountsAreEqual(Counts.Index index, Counts expectedCounts, Counts testedCounts, string countName)
		{
			Assert.AreEqual(expectedCounts.Get(index), testedCounts.Get(index), "Handler received wrong number of " + countName);
		}
	}
}
