// Copyright � 2009 Phil Booth
//
// This file is part of LibSchemaBrute.
//
// LibSchemaBrute is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// LibSchemaBrute is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibSchemaBrute. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;

namespace LibSchemaBrute
{
	class InstanceGenerator : Handler
	{
		void Handler.BeginElement(XmlSchemaElement schemaElement)
		{
			if(IgnoreChoice())
				return;

			XmlElement element = schemaElement.QualifiedName.IsEmpty ? instance.CreateElement(schemaElement.Name) : instance.CreateElement(schemaElement.Name, schemaElement.QualifiedName.Namespace);
			if(instanceContext == null)
				instance.AppendChild(element);
			else
				instanceContext.AppendChild(element);

			instanceContext = element;
			schemaContext.Push(schemaElement);
		}

		private bool IgnoreChoice()
		{
			if(ContextIsChoice())
			{
				if(!contextChoiceIsEmpty)
				{
					++ignoreCount;
					return true;
				}

				contextChoiceIsEmpty = false;
			}

			return false;
		}

		private bool ContextIsChoice()
		{
			return schemaContext.Count > 0 && schemaContext.Peek() is XmlSchemaChoice;
		}

		void Handler.EndElement()
		{
			if(ignoreCount > 0)
				--ignoreCount;
			else
			{
				XmlSchemaElement schemaElement = (XmlSchemaElement)schemaContext.Pop();
				if(!instanceContext.HasChildNodes)
					instanceContext.InnerXml = GetElementValue(schemaElement);

				enumerations.Clear();

				instanceContext = instanceContext.ParentNode as XmlElement;
			}
		}

		private string GetElementValue(XmlSchemaElement element)
		{
			return GetFixedOrDefaultValue(element.FixedValue, element.DefaultValue);
		}

		private string GetFixedOrDefaultValue(string fixedValue, string defaultValue)
		{
			if(fixedValue == null || fixedValue == "")
				return GetDefaultValueOrEnumeration(defaultValue);

			return fixedValue;
		}

		private string GetDefaultValueOrEnumeration(string defaultValue)
		{
			if(enumerations.Count == 0)
				return defaultValue == null ? "" : defaultValue;

			foreach(string value in enumerations)
			{
				if(value == defaultValue)
					return value;
			}

			// TODO: Allow user to specify which enumeration.
			return enumerations.Peek();
		}

		void Handler.BeginSequence(XmlSchemaSequence sequence)
		{
		}

		void Handler.EndSequence()
		{
		}

		void Handler.BeginChoice(XmlSchemaChoice choice)
		{
			if(IgnoreChoice())
				return;

			// TODO: Allow user to specify which choice.

			schemaContext.Push(choice);
			contextChoiceIsEmpty = true;
		}

		void Handler.EndChoice()
		{
			if(ignoreCount > 0)
				--ignoreCount;
			else
				schemaContext.Pop();
		}

		void Handler.BeginAll(XmlSchemaAll all)
		{
		}

		void Handler.EndAll()
		{
		}

		void Handler.BeginGroup(XmlSchemaGroup group)
		{
		}

		void Handler.EndGroup()
		{
		}

		void Handler.BeginSimpleType(XmlSchemaSimpleType simpleType)
		{
		}

		void Handler.EndSimpleType()
		{
		}

		void Handler.BeginSimpleTypeRestriction(XmlSchemaSimpleTypeRestriction simpleTypeRestriction)
		{
		}

		void Handler.EndSimpleTypeRestriction()
		{
		}

		void Handler.Length(XmlSchemaFacet length)
		{
		}

		void Handler.MinLength(XmlSchemaFacet minLength)
		{
		}

		void Handler.MaxLength(XmlSchemaFacet maxLength)
		{
		}

		void Handler.Pattern(XmlSchemaFacet pattern)
		{
		}

		void Handler.Enumeration(XmlSchemaFacet enumeration)
		{
			enumerations.Enqueue(enumeration.Value);
		}

		void Handler.MinInclusive(XmlSchemaFacet minInclusive)
		{
		}

		void Handler.MaxInclusive(XmlSchemaFacet maxInclusive)
		{
		}

		void Handler.MinExclusive(XmlSchemaFacet minExclusive)
		{
		}

		void Handler.MaxExclusive(XmlSchemaFacet maxExclusive)
		{
		}

		void Handler.TotalDigits(XmlSchemaFacet totalDigits)
		{
		}

		void Handler.FractionDigits(XmlSchemaFacet fractionDigits)
		{
		}

		void Handler.WhiteSpace(XmlSchemaFacet whiteSpace)
		{
		}

		void Handler.BeginSimpleTypeUnion(XmlSchemaSimpleTypeUnion union)
		{
		}

		void Handler.EndSimpleTypeUnion()
		{
		}

		void Handler.BeginSimpleTypeList(XmlSchemaSimpleTypeList list)
		{
		}

		void Handler.EndSimpleTypeList()
		{
		}

		void Handler.BeginComplexType(XmlSchemaComplexType complexType)
		{
		}

		void Handler.EndComplexType()
		{
		}

		void Handler.BeginAttribute(XmlSchemaAttribute schemaAttribute)
		{
			if(IgnoreChoice())
				return;

			XmlAttribute attribute = schemaAttribute.QualifiedName.IsEmpty ? instance.CreateAttribute(schemaAttribute.Name) : instance.CreateAttribute(schemaAttribute.QualifiedName.Name, schemaAttribute.QualifiedName.Namespace);
			((XmlElement)instanceContext).SetAttributeNode(attribute);

			instanceContext = attribute;
			schemaContext.Push(schemaAttribute);
		}

		void Handler.EndAttribute()
		{
			if(ignoreCount > 0)
				--ignoreCount;
			else
			{
				XmlSchemaAttribute schemaAttribute = (XmlSchemaAttribute)schemaContext.Pop();
				instanceContext.Value = GetAttributeValue(schemaAttribute);

				enumerations.Clear();

				instanceContext = ((XmlAttribute)instanceContext).OwnerElement;
			}
		}

		private string GetAttributeValue(XmlSchemaAttribute attribute)
		{
			return GetFixedOrDefaultValue(attribute.FixedValue, attribute.DefaultValue);
		}

		void Handler.BeginAny(XmlSchemaAny any)
		{
		}

		void Handler.EndAny()
		{
		}

		void Handler.BeginAnyAttribute(XmlSchemaAnyAttribute anyAttribute)
		{
		}

		void Handler.EndAnyAttribute()
		{
		}

		void Handler.BeginSimpleContentExtension(XmlSchemaSimpleContentExtension simpleContentExtension)
		{
		}

		void Handler.EndSimpleContentExtension()
		{
		}

		void Handler.BeginSimpleContentRestriction(XmlSchemaSimpleContentRestriction simpleContentRestriction)
		{
		}

		void Handler.EndSimpleContentRestriction()
		{
		}

		void Handler.BeginComplexContentExtension(XmlSchemaComplexContentExtension complexContentExtension)
		{
		}

		void Handler.EndComplexContentExtension()
		{
		}

		void Handler.BeginComplexContentRestriction(XmlSchemaComplexContentRestriction complexContentRestriction)
		{
		}

		void Handler.EndComplexContentRestriction()
		{
		}

		public InstanceGenerator()
		{
			Initialise(new XmlDocument(), null);
		}

		public InstanceGenerator(XmlDocument contextDocument, XmlNode contextNode)
		{
			Initialise(contextDocument, contextNode);
		}

		private void Initialise(XmlDocument contextDocument, XmlNode contextNode)
		{
			if(contextNode != null && !(contextNode is XmlElement))
				throw new ArgumentException("Invalid contextNode argument in InstanceGenerator::Initialise", "contextNode");

			instance = contextDocument;
			instanceContext = contextNode;
		}

		public XmlDocument Result
		{
			get
			{
				return instance;
			}
		}

		private XmlDocument instance;
		private XmlNode instanceContext;
		private Stack<XmlSchemaObject> schemaContext = new Stack<XmlSchemaObject>();
		bool contextChoiceIsEmpty;
		int ignoreCount = 0;
		private Queue<string> enumerations = new Queue<string>();
	}
}
